package perc_test;

import java.util.*;
import java.io.*;
import java.text.*;
import java.awt.*;
import java.awt.image.*;

import javax.imageio.*;

public class ResPerc {														//	レジストのパーコレーションモデルによるシミュレーション
	RPCell[][][] cells;														//	percolationを考えるセルの配置
 	RPResist[][][] resists;													//	露光状態を保持するデータ構造
	int[][] pattern;
 	int nx,ny,nz;
 	int nabsorbed;
	ResPercAbsorption abs;
	int numcells,numclusters;
//	RPGraph monitor;
	ResPercGraph monitor;
	ResPercRenderer rend;
	ResPercClusterOutput clusterout;
	RPReconnectDebugOutput rdebug;
	ResPercResistOutput resistout;
	PrintWriter out;
//	Random rnd;																//	乱数は現在ResPercAbsorptionの中のみで使う
	public static void main(String args[]){
		ResPerc rp=new ResPerc();
	}
	ResPerc(){
		DirectoryUtil du=new DirectoryUtil("RP");
		String path=du.create_directories();
		String pstr=du.get_pstr();											//	パス名の最後の部分を除く文字列
		du.copy_file("resist_input.xml");
		
		ResPercXmlReader reader=new ResPercXmlReader("resist_input.xml");
		nx=reader.getNx();
		ny=reader.getNy();
		nz=reader.getNz();
//		monitor=new RPGraph(nx,ny,nz);
		monitor=new ResPercGraph(nx,ny,nz);
		cells=new RPCell[nx][ny][nz];										//	positive toneでは露光領域がnullになる
		resists=new RPResist[nx][ny][nz];
		pattern=new int[nx][ny];
//		monitor.put_data(pattern);
		monitor.put_data(cells,resists);
		monitor.initialize();
		rend=monitor.getRend();
		rend.setId(du.getId());
		initialize_output(pstr);
		
		abs=new ResPercAbsorption(reader);									//	計算の初期化
		
		set_initial_profile(reader.getTone());								//	negative,positiveの時それぞれ、初期のクラスタの配置を求める
		monitor.repaint();
		LinkedList<RPCluster> clusters=new LinkedList<RPCluster>();
				
		nabsorbed=0;														//	吸収されたphotonの数
		
		for (int n=1; n<=reader.getNumphotons(); n++){
			if (n==reader.getHookstep()){
				System.out.println("halt at "+n);
			}
			RPCellIndex index=abs.get_absorption();							//	吸収が起こるセルを返す
			if (index!=null){
				nabsorbed++;
				boolean stat=exposure(n,index,reader.getTone());
				if (stat) {
					clusters=develop();										//	現像後のパターンを求める
					count_cells();
					calcLWR(n,reader.getTone());												//	line width roughnessを求める。出力も行う
				}			
			}
			if (n%reader.getOutput_interval()==0 || (n>=reader.getFine_int_from() && n<reader.getFine_int_to())) { 						//	クラスタ出力は量が大きいので出力回数を減らす
				calculate_pattern(reader.getTone());						//	露光後の上面図を得る
				rend.setStep(n);
				monitor.repaint();
				clusterout.output(n,clusters,numcells,numclusters);
				resistout.output(n);
				output_jpeg_file(pstr,n);
/*				try {
					Thread.sleep(2000);										//	描画の時間を確保するため停止する			
				}
				catch(Exception e){
					System.err.println(e);
				}*/
			}
//			if (n>8000) check_structure();
		}
		
		finalize_output();
		System.exit(0);
	}
	void set_initial_profile(boolean positive_tone){
		for (int ix=0; ix<nx; ix++){
			for (int iy=0; iy<ny; iy++){
				for (int iz=0; iz<nz; iz++){
					if (positive_tone) {
						RPCell cell=new RPCell(ix,iy,iz);					//	positive toneの時はすべてのセルが違いに結合しているとする
						cells[ix][iy][iz]=cell;
						LinkedList<RPCell> neighbors=get_neighbors(ix,iy,iz);
						new_cell(cell,neighbors);
					}
					resists[ix][iy][iz]=new RPResist();
				}
			}
		}
		develop();
	}
	boolean exposure(int n, RPCellIndex index, boolean positive_tone){
		boolean state_change_f=false;
		System.out.print(n);
		if (index!=null){
			int ix=index.getIx();
			int iy=index.getIy();
			int iz=index.getIz();
			System.out.print("\t"+ix+" "+iy+" "+iz);
			state_change_f=resists[ix][iy][iz].set_exposed();			//	状態が変わったら真。
			if (state_change_f) {										//	positive toneではセルを削除し、negative toneではセルを追加する
				if (positive_tone){
					remove_cell(n,ix,iy,iz);						
				}
				else {
					RPCell cell=new RPCell(ix,iy,iz);
					cells[ix][iy][iz]=cell;
					LinkedList<RPCell> neighbors=get_neighbors(ix,iy,iz);
					new_cell(cell,neighbors);
				}
			}
		}
		System.out.println();
		return state_change_f;
	}
	LinkedList<RPCluster> develop(){										//	現像後のパターンを求める
		LinkedList<RPCluster> clusters=find_clusters();
		reset_developed();
		for (RPCluster cluster: clusters){
			int depth=cluster.getMm().getMax();
			if (depth==nz-1) {
				cluster.getHead().set_developed(resists);
			}
/*			else {			//	そのクラスタが基盤に接触していないとき、そのクラスタが他のクラスタに接触していなければ取り除かれる
							//	別のクラスタに接触しているとき、そのクラスタが基盤に接触していれば残る
			
				boolean connected=cluster.investigate_neighbors();			//	接触しているクラスタが基盤に接触しているか調べる
				if (connected) cluster.getHead().set_developed(resists);
			}*/
		}
		return clusters;
	}
	LinkedList<RPCluster> find_clusters(){
		LinkedList<RPCluster> clusters=new LinkedList<RPCluster>();
		for (int ix=0; ix<nx; ix++){
			for (int iy=0; iy<ny; iy++){
				for (int iz=0; iz<nz; iz++){
					RPCell cell=cells[ix][iy][iz];
					if (cell!=null){
						if (cell.is_head()){
							RPCluster cluster=new RPCluster(cell,cells);
//							int depth=cell.find_max_depth();				//	クラスタの最大深さを求める。基盤に接触しているクラスタは溶解しないとみる
//							RPCluster cluster=new RPCluster(cell,depth);							
							clusters.add(cluster);
						}
					}
				}
			}
		}
		return clusters;	
	}
	void reset_developed(){
		for (int ix=0; ix<nx; ix++){
			for (int iy=0; iy<ny; iy++){
				for (int iz=0; iz<nz; iz++){
					resists[ix][iy][iz].resetDeveloped();
				}
			}
		}	
	}

	LinkedList<RPCell> get_neighbors(int ix, int iy, int iz){				//	周辺のセルに要素が存在するか調べる
		LinkedList<RPCell> neighbors=new LinkedList<RPCell>();
		if (ix<nx-1) if (cells[ix+1][iy][iz]!=null) neighbors.add(cells[ix+1][iy][iz]);
		if (ix>0)    if (cells[ix-1][iy][iz]!=null) neighbors.add(cells[ix-1][iy][iz]);
		if (iy<ny-1) if (cells[ix][iy+1][iz]!=null) neighbors.add(cells[ix][iy+1][iz]);
		if (iy>0)    if (cells[ix][iy-1][iz]!=null) neighbors.add(cells[ix][iy-1][iz]);
		if (iz<nz-1) if (cells[ix][iy][iz+1]!=null) neighbors.add(cells[ix][iy][iz+1]);
		if (iz>0)    if (cells[ix][iy][iz-1]!=null) neighbors.add(cells[ix][iy][iz-1]);
		return neighbors;
	}
	void new_cell(RPCell cell, LinkedList<RPCell> neighbors){
		if (neighbors.size()!=0){
			Iterator<RPCell> it=neighbors.iterator();						//	最初に見つかった要素の下位につける
			RPCell neighbor=it.next();
			cell.setParent(neighbor);
			neighbor.add_child(cell);
			RPCell head=neighbor.get_head();
			while(it.hasNext()){											//	さらに接続している要素がある時は、クラスタを連結する処理をする
				RPCell next=it.next();
				RPCell next_head=next.get_head();
				if (head!=next_head){
					cell.add_child(next);
					next.connect(cell);										//	見つかったクラスタを、cell以下に接続する							
				}
			}
		}
	}
	void remove_cell(int n, int ix, int iy, int iz){
		RPCell cell=cells[ix][iy][iz];
		RPCell parent=cell.getParent();
		TreeMap<String, RPCell> children=cell.getChildren();
		rdebug.output_remove_cell(n,cell);
		Iterator<String> it=children.keySet().iterator();
		while(it.hasNext()){
			RPCell child=children.get(it.next());			
			child.remove_parent();							//	子要素から親を除去する。それぞれが先頭セルとなる。
		}
		if (parent!=null) { 
			parent.remove_child(cell);						//	親要素から子を除去する
		}
		
		cells[ix][iy][iz]=null;								//	消去すべきセルから親子の情報を取得し、処理を行なったあとで、配列およびリストから要素を消去
		
		LinkedList<String> reconnect=new LinkedList<String>();	//	reconnectするセルのindexを集める
		it=children.keySet().iterator();
		while(it.hasNext()){
			reconnect.add(children.get(it.next()).get_index());
		}
		if (parent!=null) reconnect.add(parent.get_head_index());			//	上流側の断片も集める
				
		for (String index: reconnect){
			RPCell rcell=RPCell.get_cell(index,cells);
			rcell.find_reconnection(cells,rdebug);
		}
		rdebug.output_cluster_summary(cells);
	}
	void calculate_pattern(boolean positive_tone){
		for (int ix=0; ix<nx; ix++){
			for (int iy=0; iy<ny; iy++){
				pattern[ix][iy]=0;
				for (int iz=0; iz<nz; iz++){
					if (positive_tone) {
						if (!resists[ix][iy][iz].isDeveloped()) pattern[ix][iy]++;
					}
					else {
						if (resists[ix][iy][iz].isDeveloped()) pattern[ix][iy]++;
					}
				}				
			}
		}
	}
	void count_cells(){
		numcells=0;
		numclusters=0;
		for (int ix=0; ix<nx; ix++){
			for (int iy=0; iy<ny; iy++){
				for (int iz=0; iz<nz; iz++){
					if (cells[ix][iy][iz]!=null){
						numcells++;
						if (cells[ix][iy][iz].is_head()) numclusters++;
					}
				}
			}
		}
	}
	void initialize_output(String pstr){
		clusterout=new ResPercClusterOutput(pstr);
		resistout=new ResPercResistOutput(pstr,resists);
		rdebug=new RPReconnectDebugOutput(pstr);
		initialize_summary_output(pstr);
	}
	void finalize_output(){
		clusterout.finalize_output();
		resistout.finalize_output();
		rdebug.finalize_output();
		out.close();
	}
	void initialize_summary_output(String pstr){
		try {
			out=new PrintWriter(new BufferedWriter(new FileWriter(pstr+"summary.txt")));
			out.println("step\tnabs\tnumcells\tnumclusters\tnc\taverage\tsigma");
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void calcLWR(int n,boolean positive_tone){
		int[][] width=new int[nx][nz];
		int nc=0;
		for (int ix=0; ix<nx; ix++){
			for (int iz=0; iz<nz; iz++){
				int n0=0;
				int n1=nz-1;
				if (positive_tone){
					for (int iy=0; iy<ny; iy++){
						if (!resists[ix][iy][iz].isDeveloped()){
							n0=iy;
							break;
						}
					}
					for (int iy=ny-1; iy>=0; iy--){
						if (!resists[ix][iy][iz].isDeveloped()){
							n1=iy+1;
							break;
						}
					}				
				}
				else {
					for (int iy=0; iy<ny; iy++){
						if (resists[ix][iy][iz].isDeveloped()){
							n0=iy;
							break;
						}
					}
					for (int iy=ny-1; iy>=0; iy--){
						if (resists[ix][iy][iz].isDeveloped()){
							n1=iy+1;
							break;
						}
					}				
				}

				width[ix][iz]=0;
				if (n0==0 && n1==ny-1) {
					nc++;
				}
				else {
					width[ix][iz]=n1-n0;					
				}
			}
		}
		int itotal=0;
		for (int ix=0; ix<nx; ix++){
			for (int iz=0; iz<nz; iz++){
				itotal+=width[ix][iz];
			}
		}
		double average=(double)itotal/(double)(nx*nz);
		double sum=0.0;
		for (int ix=0; ix<nx; ix++){
			for (int iz=0; iz<nz; iz++){
				sum+=Math.pow((double)width[ix][iz]-average,2.0);
			}
		}
		double sigma=Math.sqrt(sum/(double)(nx*nz));
		out.println(n+"\t"+nabsorbed+"\t"+numcells+"\t"+numclusters+"\t"+nc+"\t"
				+new DecimalFormat("0.00000").format(average)+"\t"+new DecimalFormat("0.00000").format(sigma));
	}
	void output_jpeg_file(String pstr,int n){
		try {
			int panelwidth=rend.getGp().getWidth();
			int panelheight=rend.getGp().getHeight();
			BufferedImage im=new BufferedImage(panelwidth,panelheight,BufferedImage.TYPE_3BYTE_BGR);
			Graphics gc=im.getGraphics();
			rend.draw(gc);
			String gfilename=pstr+"g-"+new DecimalFormat("000000").format(n)+".jpg";
			File file=new File(gfilename);
			ImageIO.write(im,"jpeg",file);		
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void check_structure(){
		int nmax=nx*ny*nz;
		for (int ix=0; ix<nx; ix++){
			for (int iy=0; iy<ny; iy++){
				for (int iz=0; iz<nz; iz++){
					RPCell cell=cells[ix][iy][iz];
					if (cell!=null){
						int n=0;
						String idx=cell.get_index();
						while(cell.getParent()!=null){
							cell=cell.getParent();
							n++;
							if (n>nmax){
								System.out.println("structure failuer "+idx);
								break;
							}
						}						
					}
				}
			}
		}
	}
}
