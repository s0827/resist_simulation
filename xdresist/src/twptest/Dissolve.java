package twptest;

import java.util.*;
import java.io.*;
import java.text.*;

import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;

public class Dissolve {			//	DLA（ランダムウォーカー）によるレジスト溶解のモデル
	int nx=40,ny=40,nz=70;		//	システムサイズ、ハードコードから始める
	int nfilm=20;				//	レジスト膜厚
	Cell[][][] cells;
	Frame2D g2;					//	グラフィクス
	Frame3D g3;
	Renderer2D r2;

	boolean disp2=true, disp3=false;
	double max_solubility=1.0;	//	溶解度の最大値と最小値、nxの間に1周期(2π)のイメージ(潜像)があるとする
	double min_solubility=0.0;
	
	Random random;
	int nump=100000;			//	テスト粒子の数
	int zinit=40;				//	rz=zinitに初期に粒子を置く
		
	public static void main(String args[]){
		Dissolve di=new Dissolve();
	}
	Dissolve(){
		DirectoryUtil du=new DirectoryUtil("DS");
		String path=du.create_directories();
		String pstr=du.get_pstr();
		String id=du.getId();
		
		initialize();
		initialize_graphics(id);
		calculate(pstr);
	}
	void initialize_graphics(String id){			//	可視化の初期化
		if (disp2) {
			g2=new Frame2D();
			r2=new Renderer2D(cells,id);
			g2.setup_renderer(r2);			
		}		
		if (disp3){
			g3=new Frame3D();
			g3.put_cells(cells);			
		}
	}
	void initialize(){
		initialize_cells();
		initialize_resist_film();
	}
	void initialize_cells(){
		cells=new Cell[nx][ny][nz];
		for (int ix=0; ix<nx; ix++){
			for (int iy=0; iy<ny; iy++){
				for (int iz=0; iz<nz; iz++){
					cells[ix][iy][iz]=new Cell();
				}
			}
		}
	}
	void initialize_resist_film(){
		double ds=max_solubility-min_solubility;
		for (int ix=0; ix<nx; ix++){
			double p=2.0*Math.PI*(double)ix/(double)nx;
//			double s=min_solubility+(1.0-Math.pow(Math.cos(p),6.0))*ds/2.0;
			double s=Math.pow(Math.sin(p),8.0);
			for (int iy=0; iy<ny; iy++){
				for (int iz=0; iz<nfilm; iz++){
					cells[ix][iy][iz].set_resist(1.0);
					cells[ix][iy][iz].setSolubility(s);
				}
			}
		}
	}
	void calculate(String pstr){						//	ランダムウォークによる溶解過程の計算
		output_initial_profile(pstr);					//	溶解度の分布(断面)
		
		random=new Random();							//	乱数の初期化
		CellStat cellstat=new CellStat(cells,nfilm);	//	統計情報の計算
		System.out.println("step\tcells\tcells(%)\taverage\tsigma");
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(pstr+"particles.txt")));
			for (int n=0; n<nump; n++){
				random_walk(n,out);
				if (n%5000==0) {
					cellstat.calc();
					System.out.println(n+"\t"+cellstat.dispstr());
//					String line=in.readLine();
					if (g3!=null) g3.repaint();
					if (g2!=null) {
						r2.setStep(n);
						g2.repaint();
						output_jpeg_file(pstr,n);
						output_profile(pstr,n);
					}
				}
			}
			out.close();
			System.out.print("enter any key to quit");
			in.readLine();
			System.exit(0);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void random_walk(int n, PrintWriter out){
		int rx=random.nextInt(nx);
		int ry=random.nextInt(ny);
		int rz=zinit;
		int step=0;
		boolean stat=true;				//	終了判定
		out.println("particle "+n+" started from ("+rx+","+ry+","+rz+")");
		while(stat){
			int rx0=rx;					//	以前のステップの値を保存。進めなかった時に処理を戻すため
			int ry0=ry;
			int rz0=rz;
	//		System.out.println(step+" ("+rx+","+ry+","+rz+")");
			int r=random.nextInt(6);	//	次に進む方向を決める
			switch(r){
				case 0: rx++; break;
				case 1: rx--; break;
				case 2: ry++; break;
				case 3: ry--; break;
				case 4: rz++; break;
				case 5: rz--; break;
				default: break;
			}
			if (rx>=nx) rx=0;			//	周期的境界条件
			if (rx<0) rx=nx-1;
			if (ry>=ny) ry=0;
			if (ry<0) ry=ny-1;	
			if (rz>=nz) {				//	z方向で計算領域を外れたら終了
				out.println("\tout of bound "+"("+rx+","+ry+","+rz+")");
				stat=false;
				break;
			}		
			if (rz<0 || cells[rx][ry][rz].isI()){		//	z=0に達したあるいはレジスト分子がある場合は進めない
				rx=rx0;									//	移動させずに再度ランダムウォークの試行をする
				ry=ry0;
				rz=rz0;
			}
			else {
				TreeMap<Double, Cindex> neighbors=get_neighbors(rx,ry,rz);	//	隣接セルを取得。溶解判定のため
				
	 			Iterator<Double> it=neighbors.keySet().iterator();
	 			while(it.hasNext()){
	 				Cindex c=neighbors.get(it.next());
	 				Cell cell=cells[c.getRx()][c.getRy()][c.getRz()];
	 				if (random.nextDouble()<cell.getSolubility()){
	 					out.println("\tdissolution at "+" ("+c.getRx()+","+c.getRy()+","+c.getRz()+")");
	 					cell.resetI();											//	セルが溶解したとする
	 					stat=false;												//	一個のテスト粒子は一個のセルを溶解させる
	 					break;
	 				}
	 			}				
			}
			step++;
		}
	}
	TreeMap<Double, Cindex> get_neighbors(int rx,int ry,int rz){
		TreeMap<Double, Cindex> neighbors=new TreeMap<Double, Cindex>();	//	乱数をインデックスとし処理順序を決める
		int rxn=rx+1;
		if (rxn>=nx) rxn=0;
		if (cells[rxn][ry][rz].isI()) neighbors.put(random.nextDouble(),new Cindex(rxn,ry,rz));

		int rxp=rx-1;
		if (rxp<0) rxp=nx-1;
		if (cells[rxp][ry][rz].isI()) neighbors.put(random.nextDouble(),new Cindex(rxp,ry,rz));

		int ryn=ry+1;
		if (ryn>=ny) ryn=0;
		if (cells[rx][ryn][rz].isI()) neighbors.put(random.nextDouble(),new Cindex(rx,ryn,rz));

		int ryp=ry-1;
		if (ryp<0) ryp=ny-1;
		if (cells[rx][ryp][rz].isI()) neighbors.put(random.nextDouble(),new Cindex(rx,ryp,rz));
		
		int rzn=rz+1;
		if (rzn<nz) {
			if (cells[rx][ry][rzn].isI()) neighbors.put(random.nextDouble(),new Cindex(rx,ry,rzn));			
		}
		
		int rzp=rz-1;
		if (rzp>=0) {
			if (cells[rx][ry][rzp].isI()) neighbors.put(random.nextDouble(),new Cindex(rx,ry,rzp));			
		}
		return neighbors;
	}
	void output_initial_profile(String pstr){
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(pstr+"inital_profile2D.txt")));
			out.println("ix\td");
			for (int ix=0; ix<nx; ix++){
				double d=0.0;
				for (int iy=0; iy<ny; iy++){
					for (int iz=0; iz<nfilm; iz++){
						d+=cells[ix][iy][iz].getSolubility();
					}
				}
				d=d/((double)ny*(double)nfilm);
				out.println(ix+"\t"+new DecimalFormat("0.0000").format(d));
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void output_profile(String pstr, int n){
		try {
			String filename=pstr+"c-"+new DecimalFormat("000000").format(n)+".txt";
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(filename)));
			out.println("ix\th");
			for (int ix=0; ix<nx; ix++){
				int h=0;
				for (int iy=0; iy<ny; iy++){
					for (int iz=0; iz<nfilm; iz++){
						if (cells[ix][iy][iz].isI()) h++;
					}
				}
				double hav=(double)h/((double)ny*(double)nfilm);
				out.println(ix+"\t"+new DecimalFormat("0.0000").format(hav));
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void output_jpeg_file(String pstr,int n){
		try {
			int panelwidth=r2.getWidth();
			int panelheight=r2.getHeight();
			BufferedImage im=new BufferedImage(panelwidth,panelheight,BufferedImage.TYPE_3BYTE_BGR);
			Graphics gc=im.getGraphics();
			r2.draw(gc);
			String gfilename=pstr+"g-"+new DecimalFormat("000000").format(n)+".jpg";
			File file=new File(gfilename);
			ImageIO.write(im,"jpeg",file);		
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
class Cindex{
	int rx,ry,rz;
	Cindex(int rx, int ry, int rz){
		this.rx=rx;
		this.ry=ry;
		this.rz=rz;
	}
	int getRx(){ return rx; }
	int getRy(){ return ry; }
	int getRz(){ return rz; }
}
