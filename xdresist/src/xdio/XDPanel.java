package xdio;

import java.awt.*;

import xdcalc.*;

public class XDPanel extends Panel {
	int panelwidth,panelheight;
	XDcell[][][] cells;
	int nx,ny,nz;
	XDploc ploc;									//	座標の計算用
	XDgraphControl ctrl;
	Color front_color,side_color,top_color;
	XDPanel(int panelwidth, int panelheight){
		this.panelwidth=panelwidth;
		this.panelheight=panelheight;
		setSize(panelwidth,panelheight);
		setVisible(true);
	}
	void put_data(XDcell[][][] cells) {
		this.cells=cells;
		initialize();								//	polygonを
	}
	void initialize() {
		nx=cells.length;
		ny=cells[0].length;
		nz=cells[0][0].length;
		for (int ix=0; ix<nx; ix++) {
			for (int iy=0; iy<ny; iy++) {
				for (int iz=0; iz<nz; iz++) {
					XDpolygons polygons=new XDpolygons(ploc,ix,iy,iz);
					cells[ix][iy][iz].setPolygons(polygons);
				}
			}
		}		
		front_color=new Color(191,191,191);
		side_color=new Color(127,127,127);
		top_color=new Color(255,255,255);
	}
	void putPloc(XDploc ploc) { this.ploc=ploc; }
	void putCtrl(XDgraphControl ctrl) { this.ctrl=ctrl; }
	public void paint(Graphics gc) {
		if (cells==null) return;
		gc.setColor(Color.WHITE);
		gc.fillRect(0, 0, panelwidth, panelheight);
		for (int iz=0; iz<nz; iz++) {
			for (int iy=ny-1; iy>=0; iy--) {			//	視線の奥から描画する
				for (int ix=nx-1; ix>=0; ix--) {
					XDcell cell=cells[ix][iy][iz];
					if (cell.isCoated()) {
						XDpolygons polygons=cell.getPolygons();
						if (polygons!=null) {
							Polygon front=polygons.getFront();
							Polygon side=polygons.getSide();
							Polygon top=polygons.getTop();
							if (ctrl.getDimension()) {
								draw_cell(gc,front,side,top);
							}
							else {
								if (ctrl.getXf()) {
									if (ctrl.containsX(ix)) draw_side(gc,side);
								}
								if (ctrl.getYf()) {
									if (ctrl.containsY(iy)) draw_side(gc,front);
								}
								if (ctrl.getZf()) {
									if (ctrl.containsZ(iz)) draw_side(gc,top);
								}
							}
						}
					}
				}
			}
		}
	}
	void draw_side(Graphics gc, Polygon poly) {
		gc.setColor(Color.WHITE);
		gc.fillPolygon(poly);
		gc.setColor(Color.BLACK);
		gc.drawPolygon(poly);
	}
	void draw_cell(Graphics gc, Polygon front, Polygon side, Polygon top) {
		gc.setColor(front_color);
		gc.fillPolygon(front);
		gc.setColor(side_color);
		gc.fillPolygon(side);
		gc.setColor(top_color);
		gc.fillPolygon(top);
		gc.setColor(Color.BLACK);
		gc.drawPolygon(front);
		gc.drawPolygon(side);
		gc.drawPolygon(top);								
	}
}
