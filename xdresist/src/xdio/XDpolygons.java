package xdio;

import java.awt.*;

public class XDpolygons extends XDploc {		//	座標値は計算途中で変わらないのでpolygonを保存しておいた方が良い
	Polygon front,side,top;
	public XDpolygons(XDploc ploc, int ix, int iy, int iz){
		super(ploc);
		
		int x0=get_x(ix,iy,iz);					//	izはダミーとなる
		int x1=get_x(ix,iy,iz+1);
		int x2=get_x(ix+1,iy,iz+1);
		int x3=get_x(ix+1,iy,iz);
		int x4=get_x(ix,iy+1,iz);
		int x5=get_x(ix,iy+1,iz+1);
		int x6=get_x(ix+1,iy+1,iz+1);
		
		int y0=get_y(ix,iy,iz);					//	ixはダミーとなる
		int y1=get_y(ix,iy,iz+1);
		int y2=get_y(ix+1,iy,iz+1);
		int y3=get_y(ix+1,iy,iz);
		int y4=get_y(ix,iy+1,iz);
		int y5=get_y(ix,iy+1,iz+1);
		int y6=get_y(ix+1,iy+1,iz+1);
		
		front=new Polygon();					//	前面、側面、上面のポリゴンを定義
		front.addPoint(x0, y0);
		front.addPoint(x1, y1);
		front.addPoint(x2, y2);
		front.addPoint(x3, y3);
		front.addPoint(x0, y0);
		
		side=new Polygon();
		side.addPoint(x0, y0);
		side.addPoint(x4, y4);
		side.addPoint(x5, y5);
		side.addPoint(x1, y1);
		side.addPoint(x0, y0);
		
		top=new Polygon();
		top.addPoint(x1, y1);
		top.addPoint(x5, y5);
		top.addPoint(x6, y6);
		top.addPoint(x2, y2);
		top.addPoint(x1, y1);
	}
	Polygon getFront() { return front; }
	Polygon getSide() { return side; }
	Polygon getTop() { return top; }
}
