package xdio;

import java.awt.*;

public class XDploc {				//	画面上の座標を計算する
	int nx,ny,nz,dp;
	int py0;
	XDploc(){}						//	コピーコンストラクタ
	XDploc(XDploc ploc){
		this.nx=ploc.nx;
		this.ny=ploc.ny;
		this.nz=ploc.nz;
		this.dp=ploc.dp;
		initialize();
	}
	XDploc(int nx, int ny, int nz, int dp){
		this.nx=nx;
		this.ny=ny;
		this.nz=nz;
		this.dp=dp;
	}
	void initialize() {
		py0=get_panelheight();		//	反転処理用		
	}
	int get_x(int ix, int iy, int iz) {		//	3次元座標に対する2次元座標を返す
		int x=(nx-ix)*dp+iy*dp/2;
		return x;
	}
	int get_y(int ix, int iy, int iz) {
		int y=py0-iy*dp/2-iz*dp;
		return y;
	}
	int get_panelwidth() { return (nx+ny/2)*dp; }
	int get_panelheight() { return (ny/2+nz)*dp; }
}
