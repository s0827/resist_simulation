package xdio;

import java.io.*;						//	入力データの読み込み
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

public class XDInputXmlReader {
	double cx,cy,cz;					//	計算領域の大きさ、メッシュ数
	int nx,ny,nz;
	double thickness,abs_length;		//	レジストの物理パラメータ
	public XDInputXmlReader(){}
	public XDInputXmlReader(String xmlfile){
		read(xmlfile);
	}
	void read(String xmlfile) {
		try {
			InputStream is=new FileInputStream(xmlfile);
			DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
			DocumentBuilder builder=factory.newDocumentBuilder();
			Document doc=builder.parse(is);
			Element rootElement=doc.getDocumentElement();

			NodeList geometry_list=rootElement.getElementsByTagName("geometry");
			Node geometry_node=geometry_list.item(0);
			NodeList calc_region_list=((Element)geometry_node).getElementsByTagName("calc_region");
			Node calc_region_node=calc_region_list.item(0);
			NamedNodeMap calc_region_attr=calc_region_node.getAttributes();
			cx=get_double_attr(calc_region_attr,"cx");
			cy=get_double_attr(calc_region_attr,"cy");
			cz=get_double_attr(calc_region_attr,"cz");

			NodeList calc_mesh_list=((Element)geometry_node).getElementsByTagName("calc_mesh");
			Node calc_mesh_node=calc_mesh_list.item(0);
			NamedNodeMap calc_mesh_attr=calc_mesh_node.getAttributes();
			nx=get_int_attr(calc_mesh_attr,"nx");
			ny=get_int_attr(calc_mesh_attr,"ny");
			nz=get_int_attr(calc_mesh_attr,"nz");

			NodeList condition_list=rootElement.getElementsByTagName("condition");
			Node condition_node=condition_list.item(0);
			NodeList resist_list=((Element)condition_node).getElementsByTagName("resist");
			Node resist_node=resist_list.item(0);
			NamedNodeMap resist_attr=resist_node.getAttributes();
			thickness=get_double_attr(resist_attr,"thickness");
			abs_length=get_double_attr(resist_attr,"abs_length");
			is.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	double get_double_attr(NamedNodeMap map, String key) {
		double d=0.0;
		String s=get_attr(map,key);
		if (s!=null) d=Double.parseDouble(s);
		return d;
	}
	int get_int_attr(NamedNodeMap map, String key) {
		int i=0;
		String s=get_attr(map,key);
		if (s!=null) i=Integer.parseInt(s);
		return i;
	}
	String get_attr(NamedNodeMap map, String key) {
		String s=null;
		Node node=map.getNamedItem(key);
		if (node!=null) s=node.getNodeValue();
		return s;
	}
	public double get_dx() { return cx/(double)nx; }
	public double get_dy() { return cy/(double)ny; }
	public double get_dz() { return cz/(double)nz; }
	public int get_nthickness() { return (int)(thickness/get_dy()); }
	
	public double getCx() { return cx; }
	public double getCy() { return cy; }
	public double getCz() { return cz; }
	public int getNx() { return nx; }
	public int getNy() { return ny; }
	public int getNz() { return nz; }
	public double getThickness() { return thickness; }
	public double getAbs_length() { return abs_length; }
}
