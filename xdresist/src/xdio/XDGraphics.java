package xdio;

import java.util.*;
import java.awt.*;
import java.awt.event.*;

import xdcalc.*;

public class XDGraphics extends Frame implements ActionListener, ItemListener {
	int control=30;										//	コントロールを置くスペース
	int offset=20, margin=10;
	int dp=20;											//	セルの大きさの基準の値
	int framewidth=500, frameheight=500+offset;
	int panelwidth=500, panelheight=500;
	XDPanel panel;
	Checkbox D3_box,D2_box,Xd_box,Yd_box,Zd_box;
	TextField Xd_txt,Yd_txt,Zd_txt;
	Button disp_button;
	XDgraphControl ctrl;

	public XDGraphics(int nx, int ny, int nz){			//	パネルとフレームのサイズを計算する
		XDploc ploc=new XDploc(nx,ny,nz,dp);			//	描画制御用の構造を取得
		ctrl=new XDgraphControl();

		panelwidth=ploc.get_panelwidth()+1;
		panelheight=ploc.get_panelheight()+1;
		framewidth=panelwidth+2*margin;
		frameheight=panelheight+2*margin+offset+control*2;
		setSize(framewidth,frameheight);
		setVisible(true);
		setLayout(null);
		panel=new XDPanel(panelwidth,panelheight);
		panel.setLocation(margin-1,offset+4);
		panel.putPloc(ploc);
		panel.putCtrl(ctrl);
		add(panel);
		
		int hloc=10;
		int box_length=50;
		int ckb_length=40;
		int txt_length=40;
		int button_length=65;
		
		int vloc=panelheight+margin+offset+control/2-5;
		CheckboxGroup buttons=new CheckboxGroup();
		D3_box=new Checkbox("3D",buttons,true);			//	パーツの配置についての共通処理をまとめた
		place_item(D3_box,hloc,vloc,box_length,25);
		D3_box.addItemListener(this);
	
		hloc+=box_length;
		D2_box=new Checkbox("2D",buttons,false);
		place_item(D2_box,hloc,vloc,box_length,25);
		D2_box.addItemListener(this);

		hloc=10;
		vloc+=control;
		Xd_box=new Checkbox("X",null,false);
		place_item(Xd_box,hloc,vloc,ckb_length,25);
		Xd_box.addItemListener(this);
		hloc+=ckb_length;
		Xd_txt=new TextField("");
		place_item(Xd_txt,hloc,vloc,txt_length,25);
		
		hloc+=ckb_length;
		Yd_box=new Checkbox("Y",null,false);
		place_item(Yd_box,hloc,vloc,ckb_length,25);
		Yd_box.addItemListener(this);
		hloc+=ckb_length;
		Yd_txt=new TextField("");
		place_item(Yd_txt,hloc,vloc,txt_length,25);

		hloc+=ckb_length;
		Zd_box=new Checkbox("Z",null,false);
		place_item(Zd_box,hloc,vloc,ckb_length,25);
		Zd_box.addItemListener(this);
		hloc+=ckb_length;
		Zd_txt=new TextField("");
		place_item(Zd_txt,hloc,vloc,txt_length,25);
		
		hloc+=ckb_length;
		disp_button=new Button("disp");
		place_item(disp_button,hloc,vloc,button_length,25);	
		disp_button.addActionListener(this);
	}
	void place_item(Component C, int hloc, int vloc, int width, int height) {
		C.setLocation(hloc,vloc);
		C.setSize(width,height);
		add(C);
	}
	public void actionPerformed(ActionEvent e) {
		get_field_values();								//	選択されているセルのリストを随時更新
		panel.repaint();
	}
	public void itemStateChanged(ItemEvent e) {
		get_field_values();
		if (e.getSource()==D3_box){
			ctrl.setDimension(true);
		}
		else if (e.getSource()==D2_box) {
			ctrl.setDimension(false);
		}
		else if (e.getSource()==Xd_box) {
			ctrl.setXf(Xd_box.getState());
		}
		else if (e.getSource()==Yd_box) {
			ctrl.setYf(Yd_box.getState());
		}
		else if (e.getSource()==Zd_box) {
			ctrl.setZf(Zd_box.getState());
		}
		ctrl.disp();
		panel.repaint();
	}
	void get_field_values() {
		ctrl.setXi(get_list(Xd_txt.getText()));
		ctrl.setYi(get_list(Yd_txt.getText()));
		ctrl.setZi(get_list(Zd_txt.getText()));
	}
	LinkedList<Integer> get_list(String str){
		LinkedList<Integer> list=new LinkedList<Integer>();
		if (str.length()>0) {
			String[] strs=str.split("[, ]+");
			for (String s: strs) list.add(new Integer(s));
		}
		return list;
	}
	public void put_data(XDcell[][][] cells) {
		panel.put_data(cells);
	}
	public void paint(Graphics gc) {
		gc.setColor(Color.WHITE);
		gc.fillRect(0, 0, framewidth, frameheight);
		if (panel==null) return;
		panel.repaint();
	}
}
