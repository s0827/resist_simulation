package xdio;

import java.util.*;

public class XDgraphControl {
	boolean dimension=true;					//	真の時3D表示
	boolean xf=false, yf=false, zf=false;	//	各成分表示
	LinkedList<Integer> xi,yi,zi;			//	各成分のインデックス値
	XDgraphControl(){
		xi=new LinkedList<Integer>();
		yi=new LinkedList<Integer>();
		zi=new LinkedList<Integer>();
	}
	boolean containsX(int ix) { return contains(xi,ix); }
	boolean containsY(int iy) { return contains(yi,iy); }
	boolean containsZ(int iz) { return contains(zi,iz); }
	boolean contains(LinkedList<Integer> list, int i) {
		return list.contains(new Integer(i));
	}
	void disp() {
		System.out.println(dimension+" "+xf+" "+yf+" "+zf+" "+xi+" "+yi+" "+zi);
	}
	void setDimension(boolean dimension) { this.dimension=dimension; }
	void setXf(boolean xf) { this.xf=xf; }
	void setYf(boolean yf) { this.yf=yf; }
	void setZf(boolean zf) { this.zf=zf; }
	void setXi(LinkedList<Integer> xi) { this.xi=xi; }
	void setYi(LinkedList<Integer> yi) { this.yi=yi; }
	void setZi(LinkedList<Integer> zi) { this.zi=zi; }
	
	boolean getDimension() { return dimension; }
	boolean getXf() { return xf; }
	boolean getYf() { return yf; }
	boolean getZf() { return zf; }
}
