package percolation;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;

public class Percolation {
	public int nx,ny,nz;
	Cell[][][] cells;
	
	public Percolation(){}
	public void setCells(Cell[][][] cells) { this.cells=cells; }
	public void new_cell(Cell cell) {								//	セルの追加。クラスタの構造を生成する
		LinkedList<Cell> neighbors=cell.get_neighbors(cells);
		if (neighbors.size()==0) return;
		
		Cell firstneighbor=neighbors.getFirst();			//	最初に見つかった要素の下位につける
		cell.setParent(firstneighbor);
		firstneighbor.add_child(cell);
		neighbors.remove(firstneighbor);					//	残りの隣接セルについて調べる
		
		Cell head=firstneighbor.get_head();					//	さらに接続している要素がある時は、クラスタを連結する処理をする
		for (Cell nextneighbor: neighbors) {
			Cell next_head=nextneighbor.get_head();
			if (head!=next_head) {
				cell.add_child(nextneighbor);
				nextneighbor.connect(cell);					//	見つかったクラスタを、cell以下に接続する
			}
		}
	}
	public void remove_cell(Cell cell){							//	消去すべきセルから親子の情報を取得する。
		TreeMap<String, Cell> children=cell.getChildren();
		Iterator<String> it=children.keySet().iterator();
		while(it.hasNext()){
			Cell child=children.get(it.next());			
			child.remove_parent();							//	子要素から親を除去する。それぞれが先頭セルとなる。
		}
		Cell parent=cell.getParent();
		if (parent!=null) { 
			parent.remove_child(cell);						//	親要素から子を除去する
		}
		int ix=cell.getIx();								//	配列から要素を消去
		int iy=cell.getIy();
		int iz=cell.getIz();
		cells[ix][iy][iz]=null;
		Cell head=cell.get_head();
		
		LinkedList<Cell> fragments=new LinkedList<Cell>();		//	reconnectするセルを集める
		if (head!=cell) fragments.add(head);					//	headを除去した場合の処理
		it=children.keySet().iterator();
		while(it.hasNext()){
			fragments.add(children.get(it.next()));
		}
		LinkedList<Cell> targets=new LinkedList<Cell>();
		targets.addAll(fragments);
		if (targets.size()>1) {
			for (Cell fragment: fragments) {
				targets.remove(fragment);						//	reconnectする先のターゲットを決める
				boolean stat=fragment.reconnect(targets,cells);
				if (stat) targets.remove(fragment);				//	reconnectにより存在しなくなったfragmentをターゲットから除く
			}
		}
	}
	public double get_fraction(int nx, int ny, int nz) {
		int num_cell=0;
		for (int ix=0; ix<nx; ix++) {
			for (int iy=0; iy<ny; iy++) {
				for (int iz=0; iz<nz; iz++) {
					if (cells[ix][iy][iz]!=null) num_cell++;
				}
			}
		}
		double fraction=(double)num_cell/(double)(nx*ny*nz);
		return fraction;
	}
	public int get_height() {									//	[注意] x-y2次元が前提となっている
		int height=0;
		for (int ix=0; ix<cells.length; ix++) {
			for (int iy=0; iy<cells[0].length; iy++) {
				for (int iz=0; iz<cells[0][0].length; iz++) {
					if (cells[ix][iy][iz]!=null) height=Math.max(height, iy);
				}
			}
		}
		return height;
	}
}
