package percolation;

import java.util.*;
import java.awt.*;

import percolation.*;

public class TWPanel extends Panel {			// セルとクラスタの情報を描画する
	Cell[][][] cells;
	int nx,ny,dp;
	
	int panelwidth,panelheight;
	TWPanel(int panelwidth, int panelheight){
		this.panelwidth=panelwidth;
		this.panelheight=panelheight;
		setSize(panelwidth,panelheight);
		setVisible(true);
	}
	void put_parms(int nx, int ny, int dp) {
		this.nx=nx;
		this.ny=ny;
		this.dp=dp;
	}
	void setCells(Cell[][][] cells) { this.cells=cells; }
	public void paint(Graphics gc) {
		gc.setColor(Color.WHITE);
		gc.fillRect(0, 0, panelwidth, panelheight);
	
		for (int ix=0; ix<nx; ix++) {			//	セルの分布の描画
			for (int iy=0; iy<ny; iy++) {
				if (cells[ix][iy][0]!=null) {
					int x=ix*dp;
					int y=panelheight-(iy+1)*dp;
					gc.setColor(Color.LIGHT_GRAY);
					gc.fillRect(x, y, dp, dp);
				}
			}
		}		
		for (int ix=0; ix<nx; ix++) {
			for (int iy=0; iy<ny; iy++) {
				Cell cell=cells[ix][iy][0];
				if (cell!=null) {
					if (cell.is_head()) draw_cluster_structure(gc,cell);
				}
			}
		}
	}
	void draw_cluster_structure(Graphics gc, Cell cell) {
		draw_attached(gc,cell);							//	基板への接続状態の描画
		int ix=cell.getIx();
		int iy=cell.getIy();
		int dh=dp/2;
		int dq=dp/4;
		int x=ix*dp+dq;
		int y=panelheight-(iy+1)*dp+dq;
		gc.setColor(Color.BLACK);
		gc.fillOval(x, y, dh, dh);
		draw_node(gc,cell);
	}
	void draw_attached(Graphics gc, Cell cell) {
		int max_depth=cell.find_max_depth();
		if (max_depth==0) draw_attached_cell(gc,cell);
	}
	void draw_attached_cell(Graphics gc, Cell cell) {
		int x=cell.getIx()*dp;
		int y=panelheight-(cell.getIy()+1)*dp;
		gc.setColor(Color.GRAY);
		gc.fillRect(x, y, dp, dp);
		
		TreeMap<String, Cell> children=cell.getChildren();
		if (children.size()==0) return;
		
		Iterator<String> it=children.keySet().iterator();
		while(it.hasNext()) {
			Cell child=children.get(it.next());
			draw_attached_cell(gc,child);
		}
	}
	void draw_node(Graphics gc, Cell cell){				//	末端まで再帰的に線を引く
		int dh=dp/2;

		TreeMap<String, Cell> children=cell.getChildren();
		if (children.size()==0) return;
		
		int ix=cell.getIx();
		int iy=cell.getIy();
		int x=ix*dp+dh;
		int y=panelheight-(iy+1)*dp+dh;
		gc.setColor(Color.BLACK);
		Iterator<String> it=children.keySet().iterator();
		while(it.hasNext()) {
			Cell child=children.get(it.next());
			int ixc=child.getIx();
			int iyc=child.getIy();
			int xc=ixc*dp+dh;
			int yc=panelheight-(iyc+1)*dp+dh;
			gc.drawLine(x, y, xc, yc);
			draw_node(gc,child);
		}
	}
}
