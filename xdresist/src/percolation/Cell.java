package percolation;

import java.util.*;
import java.io.*;
import java.text.*;

public class Cell {									//	クラスタの構造を生成する
	int ix,iy,iz;
	Cell parent;
	int max_search_depth=3;
	TreeMap<String, Cell> children;
	public Cell(int ix, int iy, int iz){
		this.ix=ix;
		this.iy=iy;
		this.iz=iz;
		parent=null;
		children=new TreeMap<String, Cell>();
	}
	void add_child(Cell child){
		children.put(child.get_index(),child);
	}
	void remove_child(Cell child){
		String index=child.get_index();
		if (children.containsKey(index)) children.remove(index);
	}
	void remove_parent(){
		parent=null;
	}
	public void remove_cluster(Cell[][][] cells) {			//	シンプルに下位のセルを消去する
		Iterator<String> it=children.keySet().iterator();
		while(it.hasNext()) {
			Cell cell=children.get(it.next());
			cell.remove_cluster(cells);
			int ix=cell.ix;
			int iy=cell.iy;
			int iz=cell.iz;
			cells[ix][iy][iz]=null;
		}
	}
	public Cell get_head(){
		Cell head=this;
		if (parent!=null) head=parent.get_head();
		return head;
	}
	String get_head_index(){
		Cell head=get_head();
		return head.get_index();
	}
	public boolean is_head(){
		boolean status=false;
		if (parent==null) status=true;
		return status;
	}
	void connect(Cell new_parent){									//	親ノードと子ノードを入れ替える
		remove_child(new_parent);
		if (parent!=null){
			Cell old_parent=parent;
			children.put(old_parent.get_index(),old_parent);
			old_parent.connect(this);
		}
		parent=new_parent;
	}
	LinkedList<Cell> get_cluster_cells(){
		int search_depth=0;
		return get_cluster_cells(search_depth);
	}
	LinkedList<Cell> get_cluster_cells(int search_depth){
		LinkedList<Cell> ccells=new LinkedList<Cell>();
		if (search_depth>max_search_depth) return ccells;
		ccells.add(this);
		Iterator<String> it=children.keySet().iterator();
		while (it.hasNext()){
			Cell child=children.get(it.next());
			ccells.addAll(child.get_cluster_cells());
		}
		return ccells;
	}
	LinkedList<Cell> get_neighbors(Cell[][][] cells){				//	周辺のセルに要素が存在するか調べる
		int nx=cells.length;
		int ny=cells[0].length;
		int nz=cells[0][0].length;
		LinkedList<Cell> neighbors=new LinkedList<Cell>();
		if (ix<nx-1) if (cells[ix+1][iy][iz]!=null) neighbors.add(cells[ix+1][iy][iz]);
		if (ix>0)    if (cells[ix-1][iy][iz]!=null) neighbors.add(cells[ix-1][iy][iz]);
		if (iy<ny-1) if (cells[ix][iy+1][iz]!=null) neighbors.add(cells[ix][iy+1][iz]);
		if (iy>0)    if (cells[ix][iy-1][iz]!=null) neighbors.add(cells[ix][iy-1][iz]);
		if (iz<nz-1) if (cells[ix][iy][iz+1]!=null) neighbors.add(cells[ix][iy][iz+1]);
		if (iz>0)    if (cells[ix][iy][iz-1]!=null) neighbors.add(cells[ix][iy][iz-1]);
		return neighbors;
	}
	boolean reconnect(LinkedList<Cell> targets, Cell[][][] cells) {
		boolean status=false;
		targets.remove(this);
		Cell head=get_head();	
		return reconnect(targets,head,cells);
	}
	boolean reconnect(LinkedList<Cell> targets, Cell head, Cell[][][] cells) {	
		boolean status=false;
		LinkedList<Cell> neighbors=get_neighbors(cells);
		for (Cell neighbor: neighbors) {
			if (!neighbor.get_head().equals(head)) {
				neighbor.add_child(this);
				connect(neighbor);
				return true;
			}
		}
		Iterator<String> it=children.keySet().iterator();
		while(it.hasNext()) {
			Cell child=children.get(it.next());
			status=child.reconnect(targets, head,cells);
			if (status) break;
		}
		return status;
	}

	static Cell get_cell(String index, Cell[][][] cells){
		int ix=Integer.parseInt(index.substring(0,2));
		int iy=Integer.parseInt(index.substring(2,4));
		int iz=Integer.parseInt(index.substring(4,6));
		return cells[ix][iy][iz];
	}
	boolean check_head_index(Cell cell, int nz){
		boolean stat=false;
		int iz=cell.get_head().getIz();
		if (iz==nz-1) stat=true;
		return stat;
	}
	public int find_max_depth(){						//	開発用に深さy方向にとる
		return find_max_depth(iy);
	}
	public int find_max_depth(int depth){
		depth=Math.min(depth,iy);
		if (children.size()>0){
			Iterator<String> it=children.keySet().iterator();
			while(it.hasNext()){
				Cell child=children.get(it.next());
				depth=child.find_max_depth(depth);
			}
		}
		return depth;
	}
	int output_cluster(PrintWriter out){
		int sum=1;
		out.print(" "+get_index());
		if (children.size()>0) {
			if (children.size()>1) out.print(" (");
			int i=0;
			Iterator<String> it=children.keySet().iterator();
			while(it.hasNext()){
				Cell child=children.get(it.next());
				sum+=child.output_cluster(out);
				if (children.size()>1 && (i<children.size()-1)) out.print(" |");
				i++;
			}
			if (children.size()>1) out.print(" )");
		}
		return sum;
	}
	int output_cluster(){
		int sum=1;
		System.out.print(" "+get_index());
		if (children.size()>0) {
			if (children.size()>1) System.out.print(" (");
			int i=0;
			Iterator<String> it=children.keySet().iterator();
			while(it.hasNext()){
				Cell child=children.get(it.next());
				sum+=child.output_cluster();
				if (children.size()>1 && (i<children.size()-1)) System.out.print(" |");
				i++;
			}
			if (children.size()>1) System.out.print(" )");
		}
		return sum;
	}
	String get_cluster_str(){
		String str=" "+get_index();
		if (children.size()>0) {
			if (children.size()>1) str+=" (";
			int i=0;
			Iterator<String> it=children.keySet().iterator();
			while(it.hasNext()){
				Cell child=children.get(it.next());
				str+=child.get_cluster_str();
				if (children.size()>1 && (i<children.size()-1)) str+=" |";
				i++;
			}
			if (children.size()>1) str+=" )";
		}
		return str;
		
	}
	String get_index(){
		String str=new DecimalFormat("000").format(ix)+new DecimalFormat("000").format(iy)+new DecimalFormat("000").format(iz);
		return str;
	}
	int get_iindex(){
		int idx=Integer.parseInt(get_index());
		return idx;
	}
	int get_size(){
		int size=1;
		Iterator<String> it=children.keySet().iterator();
		while(it.hasNext()) size+=children.get(it.next()).get_size();
		return size;
	}
	void setParent(Cell parent){ this.parent=parent; }
	Cell getParent(){ return parent; }
	public int getIx(){ return ix; }
	public int getIy(){ return iy; }
	public int getIz(){ return iz; }
	public TreeMap<String, Cell> getChildren(){ return children; }
	
	void check_structure(Cell[][][] cells){
		int nx=cells.length;
		int ny=cells[0].length;
		int nz=cells[0][0].length;
		int nmax=nx*ny*nz;
		for (int ix=0; ix<nx; ix++){
			for (int iy=0; iy<ny; iy++){
				for (int iz=0; iz<nz; iz++){
					Cell cell=cells[ix][iy][iz];
					LinkedList<String> idxs=new LinkedList<String>();
					if (cell!=null){
						int n=0;
						String idx=cell.get_index();
						while(cell.getParent()!=null){
							cell=cell.getParent();
							idxs.add(cell.get_index());
							n++;
							if (n>nmax){
								System.out.println("structure failuer "+idx);
								System.out.println(idxs);
								System.exit(0);
							}
						}
					}
				}
			}
		}
	}
}
