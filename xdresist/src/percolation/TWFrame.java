package percolation;

import java.awt.*;

public class TWFrame extends Frame {			//	TWTestMainのためのグラフィクス
	int dp=20, offset=20;
	TWPanel panel;								//	クラスタを描画するパネルはTWmainを踏襲
	
	TWFrame(int nx, int ny, Cell[][][] cells){	//	画面の初期化
		int panelwidth=nx*dp;
		int panelheight=ny*dp;
		int framewidth=panelwidth;
		int frameheight=panelheight+offset;
		setSize(framewidth,frameheight);
		setVisible(true);
		setLayout(null);
		panel=new TWPanel(panelwidth,panelheight);
		panel.setLocation(0,offset);
		add(panel);

		panel.put_parms(nx,ny,dp);				//	パネルにデータを渡す
		panel.setCells(cells);
	}
	public void paint(Graphics gc) {			//	描画はパネルで行う
		if (panel!=null) panel.repaint();
	}
}
