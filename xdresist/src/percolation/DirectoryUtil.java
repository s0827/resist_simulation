package percolation;

import java.io.*;
import java.nio.file.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.*;

public class DirectoryUtil {		//	DirectoryUtilをJava8用に書き直す。
	protected String code="TW";
	protected String sys_id="0";
	protected String master_dir="/Users/a_sasaki/data/";
	String org_dir="originals/";
	String d_dir;
	String original,id;
	public DirectoryUtil(){
	}
	public DirectoryUtil(String code){
		this.code=code;
		Pattern pat=Pattern.compile("^[0-9]+$");
		try {
			BufferedReader in=new BufferedReader(new FileReader(master_dir+"host_id.txt"));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()) {
					code+="-"+mat.group(0)+"-";
					break;
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	public String create_directories(){
		find_directory();								//	この中でidが決まる
		Calendar calendar=Calendar.getInstance();
		int year=calendar.get(Calendar.YEAR)-2000;
		int month=calendar.get(Calendar.MONTH)+1;
		int day=calendar.get(Calendar.DAY_OF_MONTH);
		d_dir=new DecimalFormat("00").format(year)+new DecimalFormat("00").format(month)+new DecimalFormat("00").format(day);
		original=master_dir+org_dir+id;
		String sorted=master_dir+d_dir+"/";
		String project="./"+d_dir+"/";
		System.out.println(sorted+" "+original);
		System.out.println(project+" "+original);
		try {
			Files.createDirectories(Paths.get(original));
			if (!Files.exists(Paths.get(sorted))) Files.createDirectories(Paths.get(sorted)) ;
			if (!Files.exists(Paths.get(project))) Files.createDirectories(Paths.get(project)) ;
			Files.createSymbolicLink(Paths.get(sorted+id),Paths.get(original));
			Files.createSymbolicLink(Paths.get(project+id),Paths.get(original));
		}
		catch(Exception e){
			System.err.println(e);
		}
		return original;
	}
	void find_directory(){
		id="";
		int maxnum=0;
		try {
			DirectoryStream<Path> directoryStream=Files.newDirectoryStream(Paths.get(master_dir+org_dir));
			Pattern pat=Pattern.compile(get_id_str()+"\\-([0-9]+)");
			for (Path path: directoryStream){
				String s=path.toString();
				Matcher mat=pat.matcher(path.toString());
				if (mat.find()){
					int mm=Integer.parseInt(mat.group(1))+1;			//	データ保存先のディレクトリ名を取得する
					maxnum=Math.max(maxnum, mm);
				}
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
		id=get_id_str()+"-"+new DecimalFormat("0000").format(maxnum);
//		System.out.println(id);
	}
	String get_id_str(){
		return id=code+"-"+sys_id;
	}
	public void copy_file(String src){
		String dst=get_pstr()+src;
		try {
			Files.copy(Paths.get(src),Paths.get(dst));			
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	public String getId(){ return id; }					//	code,sys_id,シリアル番号からなるidを返す
	public String get_pstr(){
		return original+"/"+id+"-";
	}
	String get_pstr(String inum){					//	idを与えてファイル名を生成
		id=get_id_str()+"-"+inum;
		original=master_dir+org_dir+id;
		return get_pstr();
	}
}
