package percolation;

import java.util.*;

import java.awt.*;

import percolation.*;
import xdio.XDPanel;

public class TWmain extends Frame {					//	2次元でパーコレーションモデルのテストを行う。セルを減らす処理を確認する→これはTWTestMainで行う
	int nx=25, ny=25, nz=1;							//	メッシュ数
	Cell[][][] cells;
	
	int dp=20;										//	1セルの1辺のピクセル数
	int offset=20;
	TWPanel panel;
	public static void main(String args[]) {		//	テストプログラムなので条件はハードコードとする
		TWmain tw=new TWmain();
	}
	TWmain(){
		initialize();
		initialize_graphics();
		
		calculation();
	}
	void initialize() {
		cells=new Cell[nx][ny][1];					//	セルの初期化
	}
	void initialize_graphics() {
		int panelwidth=nx*dp;
		int panelheight=ny*dp;
		int framewidth=panelwidth;
		int frameheight=panelheight+offset;
		setSize(framewidth,frameheight);
		setVisible(true);
		setLayout(null);
		panel=new TWPanel(panelwidth,panelheight);
		panel.setLocation(0,offset);
		add(panel);
		
		panel.put_parms(nx,ny,dp);
		panel.setCells(cells);
	}
	void calculation() {
		int num=nx*ny;
		Random random=new Random();
		while(num>0) {
			int ix=random.nextInt(nx);
			int iy=random.nextInt(ny);
			int iz=0;
			if (cells[ix][iy][iz]==null) {
				Cell cell=new Cell(ix,iy,iz);		//	新しいセルの生成
				cells[ix][iy][iz]=cell;
				new_cell(cell);
				num--;
				try {
					Thread.sleep(100);
				}
				catch(Exception e) {}
//				investigate_structure(nx*ny-num);
				panel.repaint();
			}
		}
	}
	void investigate_structure(int num) {
		System.out.print(num+"> ");
		for (int ix=0; ix<nx; ix++) {
			for (int iy=0; iy<ny; iy++) {
				if (cells[ix][iy][0]!=null) {
					if (cells[ix][iy][0].is_head()) System.out.print(" ("+ix+","+iy+")");
				}
			}
		}
		System.out.println();
	}
	void new_cell(Cell cell) {								//	セルの追加。クラスタの構造を生成する
		LinkedList<Cell> neighbors=get_neighbors(cell);
		if (neighbors.size()==0) return;
		
		Cell firstneighbor=neighbors.getFirst();			//	最初に見つかった要素の下位につける
		cell.setParent(firstneighbor);
		firstneighbor.add_child(cell);
		neighbors.remove(firstneighbor);					//	残りの隣接セルについて調べる
		
		Cell head=firstneighbor.get_head();					//	さらに接続している要素がある時は、クラスタを連結する処理をする
		for (Cell nextneighbor: neighbors) {
			Cell next_head=nextneighbor.get_head();
			if (head!=next_head) {
				cell.add_child(nextneighbor);
				nextneighbor.connect(cell);					//	見つかったクラスタを、cell以下に接続する
			}
		}
	}
	LinkedList<Cell> get_neighbors(Cell cell){				//	周辺のセルに要素が存在するか調べる
		int ix=cell.getIx();
		int iy=cell.getIy();
		int iz=cell.getIz();
		LinkedList<Cell> neighbors=new LinkedList<Cell>();
		if (ix<nx-1) if (cells[ix+1][iy][iz]!=null) neighbors.add(cells[ix+1][iy][iz]);
		if (ix>0)    if (cells[ix-1][iy][iz]!=null) neighbors.add(cells[ix-1][iy][iz]);
		if (iy<ny-1) if (cells[ix][iy+1][iz]!=null) neighbors.add(cells[ix][iy+1][iz]);
		if (iy>0)    if (cells[ix][iy-1][iz]!=null) neighbors.add(cells[ix][iy-1][iz]);
		if (iz<nz-1) if (cells[ix][iy][iz+1]!=null) neighbors.add(cells[ix][iy][iz+1]);
		if (iz>0)    if (cells[ix][iy][iz-1]!=null) neighbors.add(cells[ix][iy][iz-1]);
		return neighbors;
	}

	public void paint(Graphics gc) {
		if (panel!=null) panel.repaint();
	}
}
