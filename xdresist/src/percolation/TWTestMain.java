package percolation;

import java.util.*;
import java.io.*;

public class TWTestMain extends Percolation {		//	レジスト露光、現像モデルのパーコレーションの扱いについてのテスト計算
//	int nx=20, ny=20, nz=1;							//	親クラスで定義する
//	Cell[][][] cells;
	
	TWFrame frame;
	Random random;
	public static void main(String args[]) {		//	潜像を作ってそれが溶解する過程に相当する計算のテスト
		TWTestMain twt=new TWTestMain();			//	実際の問題の計算ができるための発展性を持たせる
	}
	TWTestMain(){
		DirectoryUtil du=new DirectoryUtil();		//	データ出力用ディレクトリの作成
		String path=du.create_directories();
		String pstr=du.get_pstr();
		
		nx=20;										//	システムサイズの設定
		ny=20;
		nz=1;

		cells=new Cell[nx][ny][1];					//	セルの初期化
		frame=new TWFrame(nx,ny,cells);				//	グラフィクスの初期化
		
		calculation();
	}
	void calculation() {							//	対話型処理
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Use initial profile by id (or calculate) > ");
			String line=in.readLine();
			
			random=new Random(0);
			
			create_initial_cluster();				//	初期構造を生成
			
			int num=nx*ny;
			int i=0;
			while(num>0) {
				int ix=random.nextInt(nx);
				int iy=random.nextInt(ny);
				int iz=0;
				if (cells[ix][iy][iz]!=null) {
					System.out.print(i+"> ");
					i++;
					in.readLine();
					Cell cell=cells[ix][iy][iz];	//	新しいセルの生成
					remove_cell(cell);
					num--;
					frame.repaint();

				}				
			}
			
			in.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void create_initial_cluster() {
		int num=nx*ny;
		System.out.print("creating cluster...");
		while(num>0) {
			int ix=random.nextInt(nx);
			int iy=random.nextInt(ny);
			int iz=0;
			if (cells[ix][iy][iz]==null) {
				Cell cell=new Cell(ix,iy,iz);			//	新しいセルの生成
				cells[ix][iy][iz]=cell;
				new_cell(cell);
				num--;
			}
		}
		System.out.println("done");
		frame.repaint();
	}
}
