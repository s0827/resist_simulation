package exdptest;

import percolation.*;

public class Resist extends Cell {			//	Cellにレジスト分子の情報を付与
	boolean grassed=false;
	boolean exposed=false;
	boolean simple_percolation=false;		//	露光過程のみを考えたパーコレーションモデルでのrelief image
	public Resist(int ix, int iy, int iz){
		super(ix,iy,iz);	
	}
	public boolean is_attached() {			//	基板と接続しているか iy=0 で判断する
		boolean stat=false;
		Cell head=get_head();
		if (head.find_max_depth()==0) stat=true;
		return stat;
	}
	void setExposed(boolean exposed) { this.exposed=exposed; }
	public void setGrassed(boolean grassed) { this.grassed=grassed; }
	void setSimple_percolation(boolean simple_percolation) { this.simple_percolation=simple_percolation; }
	public boolean isExposed() { return exposed; }
	public boolean isGrassed() { return grassed; }
	public boolean isSimple_percolation() { return simple_percolation; }
}
