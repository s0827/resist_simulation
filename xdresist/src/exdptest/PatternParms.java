package exdptest;

public class PatternParms {				//	パターンの特性。データ受け渡し用
	double average,sigma;
	public PatternParms(double average, double sigma){
		this.average=average;
		this.sigma=sigma;
	}
	public double getAverage() { return average; }
	public double getSigma() { return sigma; }
}
