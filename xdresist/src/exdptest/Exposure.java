package exdptest;

import java.util.*;
import percolation.*;								//	従来のパーコレーションシミュレーションでのイメージとの比較を行う

public class Exposure extends Percolation {			//	今回は露光はランダム
	Resist[][][] cells;
	Cell[][][] xcells;
	int nx,ny,nz;
	LinkedList<Integer> exposed_area;				//	露光領域。パターンをここに定義する
	public Exposure(Resist[][][] cells){
		this.cells=cells;
		nx=cells.length;
		ny=cells[0].length;
		nz=cells[0][0].length;
		xcells=new Cell[nx][ny][nz];
		setCells(xcells);
		exposed_area=new LinkedList<Integer>();
		for (int i=0; i<nx; i++) exposed_area.add(new Integer(i));		//	デフォルトでは全領域露光する
	}
	public void define_exposed_area(int numlines, double ratio) {
		if (numlines==0) return;										//	全面露光の場合
		exposed_area=new LinkedList<Integer>();
		int[] np=new int[numlines+1];
		int[] nb=new int[numlines];
		int dnp=divide(nx,numlines);									//	割り切れない時警告し、停止する
		for (int i=0; i<=numlines; i++) np[i]=dnp*i;
		for (int i=0; i<numlines; i++) nb[i]=divide(np[i]+np[i+1],2);
		int r=(int)(1.0/ratio);
		int d=divide(nx,2*r*numlines);
		for (int i=0; i<numlines; i++) {
			int j0=nb[i]-d;
			int j1=nb[i]+d;
			for (int j=j0; j<j1; j++) exposed_area.add(new Integer(j));
		}
	}
	int divide(int n, int m) {
		int result=n/m;
		int prod=result*m;
		if (n!=prod) {
			System.out.println("indivisible: "+n+" "+m);
			System.exit(0);
		}
		return result;
	}
	public void calculation(int thickness, double packing_ratio, Random random) {
		int num=(int)((double)(exposed_area.size()*thickness)*packing_ratio);
	
		while(num>0) {								//	感光したセルの基板とのつながりを評価する
			int ix=random.nextInt(nx);
			if (exposed_area.contains(new Integer(ix))) {
				int iy=random.nextInt(thickness);
				int iz=0;
				Resist cell=cells[ix][iy][iz];
				if (!cell.isExposed()) {
					cell.setExposed(true);
					Cell xcell=new Cell(ix,iy,iz);
					xcells[ix][iy][iz]=xcell;
					new_cell(xcell);			
					num--;
				}
			}
		}
		
		LinkedList<Cell> heads=new LinkedList<Cell>();		//	基板と繋がっているクラスタを探す
		for (int ix=0; ix<nx; ix++) {
			for (int iy=0; iy<ny; iy++) {
				for (int iz=0; iz<nz; iz++) {
					Cell xcell=xcells[ix][iy][iz];
					if (xcell!=null) {
						if (xcell.is_head()){
							if (xcell.find_max_depth()==0) heads.add(xcell);
						}						
					}
				}
			}
		}		
		for (Cell cell: heads) {
			define_image(cell);
		}
	}
	void define_image(Cell cell) {					//	head以下のセルについて基板との繋がりの情報をコピー
		int ix=cell.getIx();
		int iy=cell.getIy();
		int iz=cell.getIz();
		cells[ix][iy][iz].setSimple_percolation(true);
		TreeMap<String, Cell> children=cell.getChildren();
		
		Iterator<String> it=children.keySet().iterator();
		while(it.hasNext()) {
			Cell child=children.get(it.next());
			define_image(child);
		}
	}
	public double get_fraction(int nx, int ny, int nz) {
		int num_cell=0;
		for (int ix=0; ix<nx; ix++) {
			for (int iy=0; iy<ny; iy++) {
				for (int iz=0; iz<nz; iz++) {
					Resist cell=cells[ix][iy][iz];
					if (cell!=null) {
						if (cell.isSimple_percolation()) num_cell++;
					}
				}
			}
		}
		double fraction=(double)num_cell/(double)(nx*ny*nz);
		return fraction;
	}
	public int get_height() {
		int height=0;
		for (int ix=0; ix<cells.length; ix++) {
			for (int iy=0; iy<cells[0].length; iy++) {
				for (int iz=0; iz<cells[0][0].length; iz++) {
					Resist cell=cells[ix][iy][iz];
					if (cell!=null) {
						if (cell.isSimple_percolation()) height=Math.max(height, iy);
					}
				}
			}
		}
		return height;
	}
	public LinkedList<Integer> getExposed_area() { return exposed_area; }
}

