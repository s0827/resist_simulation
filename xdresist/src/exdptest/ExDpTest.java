package exdptest;

import percolation.*;

import java.util.*;

import io.EDTFrame;
import io.EDTStatus;

import java.io.*;
import java.text.*;

public class ExDpTest extends Percolation {			//	２次元で露光と現像の計算のテストを行う
//	int nx=80, ny=80, nz=1;							//	条件はハードコードとする。宣言は親クラスにある。
	int thickness=20;								//	レジスト膜厚
	int startpoint=50;								//	ランダムウォーカーが出発する場所
	int limitheight=80;								//	ランダムウォーカーの位置の制限、グラフィクス表示の描画領域外
	double packing_ratio=0.6;						//	露光されたレジストの割合。ランダムに分布するとする。
	Resist[][][] cells;
	
	EDTFrame frame;
	EDTStatus stat;
	Random random;
	
	int total_steps=0;								//	全モンテカルロステップ数
	
	DirectoryUtil du;
	PrintWriter out;
	
	public static void main(String args[]) {
		ExDpTest edt=new ExDpTest();
	}
	ExDpTest(){
		du=new DirectoryUtil();						//	データ出力用ディレクトリの作成
		String path=du.create_directories();
		
		nx=40;
		ny=40;
		nz=1;
		cells=new Resist[nx][ny][nz];
		setCells(cells);
		stat=new EDTStatus();
		frame=new EDTFrame(nx,ny,cells);
		frame.setStat(stat);
		
		initialize_output();
		
		calculation();

		out.close();
	}
	void calculation() {
		random=new Random(0);
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			coating();
			Exposure exposure=exposure();
			double xfraction=exposure.get_fraction(nx,thickness,nz);
			int xmax_y=exposure.get_height();
			
			development(in);
			double fraction=get_fraction(nx,thickness,nz);		//	残膜厚の計算
			int max_y=get_height();
			
			output_summary(xfraction,fraction,xmax_y,max_y);
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void coating() {								//	レジスト膜のコーティング、クラスタの構造の生成のためで見せかけのもの
		int num=nx*thickness;
		System.out.print("coating...");
		while(num>0) {
			int ix=random.nextInt(nx);
			int iy=random.nextInt(thickness);
			int iz=0;
			if (cells[ix][iy][iz]==null) {
				Resist cell=new Resist(ix,iy,iz);
				cells[ix][iy][iz]=cell;
				new_cell(cell);
				num--;
			}
		}
		System.out.println("done");
//		frame.repaint();
	}
	Exposure exposure() {
		System.out.print("exposure...");
		Exposure exposure=new Exposure(cells);										//	従来のシミュレーションとの比較のためのデータを取得する
		exposure.calculation(thickness, packing_ratio,random);		//	cellsの内容が更新される
		System.out.println("done");
		frame.repaint();
		
		return exposure;
	}
	void development(BufferedReader in) {
		total_steps=0;
		try {
			int n=0;
			int ndisp=0;
			while(true) {						//	DSAモデルによるレジスト膜溶解の計算。テスト粒子ごとのループ
				boolean status=random_walk(n++);
				if (status) {
					remove_clusters();			//	基板に接続していないクラスタを取り除く
					output(n);
					ndisp++;
					if (ndisp%10==0) {
						frame.repaint();
						Thread.sleep(300);
					}
				}
				if (n==10000) {
					break;
				}
			}
			output(n);
			frame.repaint();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	boolean random_walk(int n) {
		boolean status=false;					//	セルの除去が行われて状況が変わったら真を返す
		int x=random.nextInt(nx);
		int y=startpoint;
		int z=0;
		
		System.out.print("partile "+n+" : started from "+x+","+y+" ");
		stat.setStep(n);
		
		while(true) {							//	ランダムウォーク
			total_steps++;
			int x0=x;							//	ロールバックの処理のため前回のステップの値を保存
			int y0=y;
			int z0=z;
			
			switch(random.nextInt(4)){
				case 0: x++; break;
				case 1: x--; break;
				case 2: y++; break;
				case 3: y--; break;
				default: break;
			}
			if (x>=nx) x=x0;
			if (x<0) x=x0;
			if (y>=limitheight || y<0) {
				System.out.print(" out of bound "+x+","+y+" ");
				break;
			}
			if (y<=ny-1) {							//	ダミーの領域中は処理しない
				Resist cell=cells[x][y][z];
				if (cell!=null) {					//	レジスト膜が存在する場合
					if (cell.isExposed()) {			//	ネガ型レジストでは感光した部分は溶解しない。ロールバックする
						x=x0;
						y=y0;
						z=z0;
					}
					else {
						cells[x][y][z]=null;		//	セルを取り除く（溶解したとする）
						remove_cell(cell);
						System.out.print(" dissolved "+x+","+y+" ");
						status=true;
						break;
					}
				}						
			}
		}
		System.out.println();
		return status;
	}
	void remove_clusters() {
		for (int ix=0; ix<nx; ix++) {
			for (int iy=0; iy<ny; iy++) {
				for (int iz=0; iz<nz; iz++) {
					Resist cell=cells[ix][iy][iz];
					if (cell!=null) {
						if (cell.is_head()) {
							if (cell.find_max_depth()!=0) {
								cell.remove_cluster(cells);
								cells[ix][iy][iz]=null;
							}
						}
					}
				}
			}
		}
	}
	void initialize_output() {
		String pstr=du.get_pstr();
		try {
			out=new PrintWriter(new BufferedWriter(new FileWriter(pstr+"dissolution_rate.txt")));
			out.println("num\ttotal_step\tmass_fraction");
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void output(int n) {
		int num_cells=0;
		for (int ix=0; ix<nx; ix++) {
			for (int iy=0; iy<ny; iy++) {
				for (int iz=0; iz<nz; iz++) {
					if (cells[ix][iy][iz]!=null) num_cells++;
				}
			}
		}
		double fraction=(double)num_cells/(double)(nx*ny*nz);
		out.println(n+"\t"+total_steps+"\t"+new DecimalFormat("0.00000").format(fraction));
	}
	void output_summary(double xfraction, double fraction, int xmax_y, int max_y) {
		String pstr=du.get_pstr();
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(pstr+"summary.txt")));
			out.println("packing_ratio        = "+new DecimalFormat("0.00000").format(packing_ratio));
			out.println("height  (percolation)= "+xmax_y);
			out.println("height  (dissolution)= "+max_y);
			out.println("fraction(percolation)= "+new DecimalFormat("0.00000").format(xfraction));
			out.println("fraction(dissolution)= "+new DecimalFormat("0.00000").format(fraction));
			out.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
}
