package exdptest;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

import exdpproto.*;
import io.*;
import percolation.Percolation;

public class Development extends Percolation {
	EDPdirectoryUtil du;
	
	Resist[][][] cells;	
	EDTFrame frame;
	EDTStatus stat;
	Random random;
	int nx,ny,nz;								//	セルのパラメータ、系のサイズ
	int startpoint,limitheight;					//	DLAの制御パラメータ
	int total_steps,max_steps,profout_steps;
	double idisprob,xdisprob;					//	感光、未感光領域の溶解確率
	
	PrintWriter out;
	
	public Development(ExDpProto edp){			//	メインプログラムからパラメータを受け取る
		cells=edp.getCells();
		setCells(cells);						//	percolationクラスにデータを渡す
		frame=edp.getFrame();
		stat=edp.getStat();
		random=edp.getRandom();
		
		EDPInputXmlReader reader=edp.getReader();
		nx=reader.getNx();
		ny=reader.getNy();
		nz=reader.getNz();
		startpoint=reader.getStartpoint();
		limitheight=reader.getLimitheight();
		idisprob=reader.getIdisprob();
		xdisprob=reader.getXdisprob();
		
		max_steps=reader.getMax_steps();
		profout_steps=reader.getProfout_steps();
	
		du=edp.getDu();
	}
	public void calculation(EDPOutputProfile profout) {
		initialize_output();
		
		total_steps=0;
		try {
			int n=0;
			int ndisp=0;
			while(true) {						//	DSAモデルによるレジスト膜溶解の計算。テスト粒子ごとのループ
				boolean status=random_walk(n++);
				if (status) {
					remove_clusters();			//	基板に接続していないクラスタを取り除く
					output(n);
					ndisp++;
					if (ndisp%10==0) {
						frame.repaint();
						Thread.sleep(300);
					}
					if (ndisp%profout_steps==1) {
						profout.output(n,total_steps);
//						System.out.print(" ");
					}
				}
				if (n>=max_steps) {
					profout.output(n,total_steps);
					break;
				}
			}
			output(n);
			frame.repaint();
		}
		catch(Exception e) {
			System.err.println(e);
		}
		
		out.close();
	}
	boolean random_walk(int n) {
		boolean status=false;					//	セルの除去が行われて状況が変わったら真を返す
		int x=random.nextInt(nx);
		int y=startpoint;
		int z=0;
		
		System.out.print("partile "+n+" : started from "+x+","+y+" ");
		stat.setStep(n);
		
		while(true) {							//	ランダムウォーク
			total_steps++;
			int x0=x;							//	ロールバックの処理のため前回のステップの値を保存
			int y0=y;
			int z0=z;
			
			switch(random.nextInt(4)){
				case 0: x++; break;
				case 1: x--; break;
				case 2: y++; break;
				case 3: y--; break;
				default: break;
			}
			if (x>=nx) x=x0;
			if (x<0) x=x0;
			if (y>=limitheight || y<0) {
				System.out.print(" out of bound "+x+","+y+" ");
				break;
			}
			if (y<=ny-1) {							//	ダミーの領域中は処理しない
				Resist cell=cells[x][y][z];
				if (cell!=null) {					//	レジスト膜が存在する場合
					boolean dissolve=false;
					if (!cell.isGrassed()) {		//	ガラス化しているセルは溶解しない
						if (cell.isExposed()) {
							if (xdisprob==1.0) {
								dissolve=true;
							}
							else {
								if (random.nextDouble()<xdisprob) dissolve=true;
							}
						}
						else {
							if (idisprob>0.0) {
								if (random.nextDouble()<idisprob) dissolve=true;
							}
						}				
					}
					if (dissolve) {					//	ネガ型レジストでは感光した部分は溶解しない。ロールバックする
						cells[x][y][z]=null;		//	セルを取り除く（溶解したとする）
						remove_cell(cell);
						System.out.print(" dissolved "+x+","+y+" ");
						status=true;
						break;
					}
					else {
						x=x0;
						y=y0;
						z=z0;
					}
				}						
			}
		}
		System.out.println();
		return status;
	}
	void remove_clusters() {
		for (int ix=0; ix<nx; ix++) {
			for (int iy=0; iy<ny; iy++) {
				for (int iz=0; iz<nz; iz++) {
					Resist cell=cells[ix][iy][iz];
					if (cell!=null) {
						if (cell.is_head()) {
							if (cell.find_max_depth()!=0) {
								cell.remove_cluster(cells);
								cells[ix][iy][iz]=null;
							}
						}
					}
				}
			}
		}
	}
	void initialize_output() {
		String pstr=du.get_pstr();
		try {
			out=new PrintWriter(new BufferedWriter(new FileWriter(pstr+"dissolution_rate.txt")));
			out.println("num\ttotal_step\tmass_fraction");
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void output(int n) {
		int num_cells=0;
		for (int ix=0; ix<nx; ix++) {
			for (int iy=0; iy<ny; iy++) {
				for (int iz=0; iz<nz; iz++) {
					if (cells[ix][iy][iz]!=null) num_cells++;
				}
			}
		}
		double fraction=(double)num_cells/(double)(nx*ny*nz);
		out.println(n+"\t"+total_steps+"\t"+new DecimalFormat("0.00000").format(fraction));
	}
	public PatternParms calc_pattern_height(LinkedList<Integer> exposed_area) {					//	[注意] x-y2次元が前提となっている
		int[] height=new int[cells.length];
		for (int ix=0; ix<cells.length; ix++) {
			height[ix]=0;
			if (exposed_area.contains(new Integer(ix))) {
				for (int iy=0; iy<cells[0].length; iy++) {
					for (int iz=0; iz<cells[0][0].length; iz++) {
						if (cells[ix][iy][iz]!=null) height[ix]=Math.max(height[ix],iy+1);
					}
				}
			}
		}
		double average=0.0;
		for (int ix=0; ix<height.length; ix++) average+=(double)height[ix];
		average=average/(double)exposed_area.size();
		double sigma=0.0;
		for (int ix=0; ix<height.length; ix++) {
			if (exposed_area.contains(new Integer(ix))) sigma+=Math.pow((double)height[ix]-average,2.0);
		}
		sigma=Math.sqrt(sigma/(double)exposed_area.size());
		
		PatternParms pp=new PatternParms(average,sigma);
		return pp;
	}

}
