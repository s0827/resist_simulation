package xdcalc;

import xdio.*;

public class XDMain {
	XDInputXmlReader reader;
	XDGraphics frame;
	
	XDcell[][][] cells;
	
	public static void main(String args[]) {
		XDMain xd=new XDMain();
	}
	XDMain(){
		reader=new XDInputXmlReader("XDinput.xml");
		initialize();																		//	初期化
		
		frame=new XDGraphics(reader.getNx(),reader.getNy(),reader.get_nthickness());		//	z方向はレジスト膜のあるとこまで描画対象とする
	
		frame.put_data(cells);		
		frame.repaint();
	}
	void initialize() {													//	レジスト膜の初期化
		cells=new XDcell[reader.getNx()][reader.getNy()][reader.getNz()];
		double dx=reader.getCx()/(double)reader.getNx();
		double dy=reader.getCy()/(double)reader.getNy();
		double dz=reader.getCz()/(double)reader.getNz();
		for (int ix=0; ix<reader.getNx(); ix++) {
			for (int iy=0; iy<reader.getNy(); iy++) {
				for (int iz=0; iz<reader.getNz(); iz++) {				//	アドレスの値などの情報を書き込む
					double x=dx*(double)ix;
					double y=dy*(double)iy;
					double z=dz*(double)iz;
					XDcell cell=new XDcell(ix,iy,iz,x,y,z);
					cells[ix][iy][iz]=cell;
					if (iz<reader.get_nthickness()) cell.setCoated();
				}
			}
		}
	}
}
