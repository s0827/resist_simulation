package xdcalc;

import xdio.*;

public class XDcell {				//	セルの状態。negative toneレジストを想定
	boolean coated=false;			//	塗布されると真
	boolean exposed=false;			//	感光すると真	
	boolean dissolved=false;		//	溶解すると真
	int ix,iy,iz;
	double x,y,z;
	XDpolygons polygons;			//	描画用の座標データを保存する
	XDcell(int ix, int iy, int iz, double x, double y, double z){
		this.ix=ix;
		this.iy=iy;
		this.iz=iz;
		this.x=x;
		this.y=y;
		this.z=z;
	}
	void setCoated(){ coated=true; }
	public void setPolygons(XDpolygons polygons) { this.polygons=polygons; }
	public boolean isCoated() { return coated; }
	public XDpolygons getPolygons() { return polygons; }
}
