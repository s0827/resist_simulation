package exdpproto;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;
import javax.imageio.ImageIO;

import io.EDTPanel;

public class EDPGraphics extends Frame {			// グラフィック出力、検証用
	String path="/Users/a_sasaki/data/originals/";
	String idstr="XD-0-";
	int nx,ny,nz;
	int dp=8, offset=20;
	int framewidth,frameheight;
	String key="";
	String id="";
	
	EDPPanel panel;
	EDPRenderer rend;
	
	public static void main(String args[]) {
		EDPGraphics edpg=new EDPGraphics();
	}
	EDPGraphics(){
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			System.out.print("enter id (XD-0-nnnn) " );
			id=idstr+in.readLine();
			LinkedHashMap<String, String[][][]> Data=read_file(id);

			initialize(Data);		//	データサイズはファイルから読み込む
			
			LinkedList<String> list=new LinkedList<String>();
			list.addAll(Data.keySet());
			ListIterator<String> it=list.listIterator();
			rend.setId(id);
			key=it.next();
			while(true) {
				rend.setKey(key);
				repaint();
				System.out.print("> ");
				String line=in.readLine();
				if (line.toUpperCase().equals("Q")) {
					System.exit(0);
				}
				else if (line.toUpperCase().equals("N")) {
					if (it.hasNext()) key=it.next();
				}
				else if (line.toUpperCase().equals("P")) {
					if (it.hasPrevious()) key=it.previous();
				}
				else if (line.toUpperCase().equals("A")) {
					animation(list,id);
				}
				else if (line.toUpperCase().equals("O")) {			//	一個の画像ファイルを生成
					output_jpeg_file();
				}
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	LinkedHashMap<String, String[][][]> read_file(String id){
		Pattern pat=Pattern.compile("steps=\t([0-9]+)");
		LinkedHashMap<String, String[][][]> Data=new LinkedHashMap<String, String[][][]>();	
		try {
			String filename=path+id+"/"+id+"-profile.txt";
			BufferedReader in=new BufferedReader(new FileReader(filename));
			in.readLine();
			nx=get_first_word(in.readLine());
			ny=get_first_word(in.readLine());
			nz=get_first_word(in.readLine());
			System.out.println("data size=("+nx+","+ny+","+nz+")");
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()) {
					String steps=mat.group(1);
					String[][][] S=read_block(in,nx,ny,nz);
					write_data(S);
					Data.put(steps,S);
				}
			}
			in.close();
			System.out.println(Data.size()+" data blocks read");
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return Data;
	}
	int get_first_word(String line) {
		String[] strs=line.split("\t");
		int num=0;
		if (strs[0]!=null) num=Integer.parseInt(strs[0]);
		return num;
	}
	String[][][] read_block(BufferedReader in, int nx, int ny, int nz){
		String[][][] S=new String[nx][ny][nz];
		for (int iz=0; iz<nz; iz++) {
			for (int iy=0; iy<ny; iy++) {
				try {			
					String line=in.readLine();
					for (int ix=0; ix<nx; ix++) {
						S[ix][iy][iz]=line.substring(ix,ix+1);
					}
				}
				catch(Exception e) {
					System.err.println(e);
				}					
			}
		}
		return S;
	}
	void initialize(LinkedHashMap<String, String[][][]> Data) {
		int panelwidth=nx*dp;
		int panelheight=ny*dp;
		framewidth=panelwidth;
		frameheight=panelheight+offset;
		setSize(framewidth,frameheight);
		setVisible(true);
		setLayout(null);
		rend=new EDPRenderer(nx,ny,dp,Data);
		panel=new EDPPanel(rend, panelwidth,panelheight);
		panel.setLocation(0,offset);
		add(panel);
	}
	void write_data(String[][][] S) {
		for (int iz=0; iz<S[0][0].length; iz++) {
			for (int iy=0; iy<S[0].length; iy++) {
				for (int ix=0; ix<S.length; ix++) {
					System.out.print(S[ix][iy][iz]);
				}
				System.out.println();
			}
		}
		System.out.println();
	}
	void output_jpeg_file() {
		int panelwidth=panel.panelwidth;
		int panelheight=panel.panelheight;
		try {
			BufferedImage im=new BufferedImage(panelwidth,panelheight,BufferedImage.TYPE_3BYTE_BGR);
			Graphics2D g2=(Graphics2D)im.getGraphics();
			rend.draw(g2);
			String filename=path+id+"/"+id+"-graph.jpg";
			File file=new File(filename);
			ImageIO.write(im,"jpeg",file);		
			
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	public void paint(Graphics gc) {
		gc.setColor(Color.WHITE);
		gc.fillRect(0, 0, framewidth, frameheight);
		if (panel!=null) panel.repaint();
	}
	void animation(LinkedList<String> list, String id) {
		String directory=make_directory(id);
		int num=0;
		for (String key: list) {
			try {
				rend.setKey(key);
				repaint();
				Thread.sleep(100);
				
				if (list.size()<10000) {
					int panelwidth=rend.getPanelwidth();
					int panelheight=rend.getPanelheight();
					BufferedImage im=new BufferedImage(panelwidth,panelheight,BufferedImage.TYPE_3BYTE_BGR);
					Graphics2D g2=(Graphics2D)im.getGraphics();
					rend.draw(g2);
					String filename=directory+id+"-g-"+new DecimalFormat("0000").format(num++)+".jpeg";
					File file=new File(filename);
					ImageIO.write(im, "jpeg", file);
				}
			}
			catch(Exception e) {
				System.err.println(e);
			}
		}
	}
	String make_directory(String id) {
		String directory=path+id+"/jpeg/";
		try {
			File dir=new File(directory);
			dir.mkdirs();
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return (directory);
	}
}
