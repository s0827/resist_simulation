package exdpproto;

import io.*;

import java.io.*;
import java.util.*;
import java.text.*;

import exdptest.*;
import percolation.*;

public class ExDpProto extends Percolation {		//	プロトタイプの計算で、2次元で簡単なパラメータスタディを行う	
	EDPdirectoryUtil du;							//	レジストは全体のメッシュの一部の位置のみに置く
	EDPInputXmlReader reader;
	EDPOutputProfile profout;
	String xmlfile="ExDpinput.xml";
	
	int thickness;									//	レジスト膜厚
	double exp_fraction;							//	感光される領域の割合
	int numlines;									//	溝の本数
	double ratio;									//	溝の割合
//	int startpoint,limitheight;						//	DLAの制御パラメータ。主プログラムでは不要
	Resist[][][] cells;								//	レジスト膜のデータ
	
	EDTFrame frame;									//	グラフィクス
	EDTStatus stat;
	Random random;

	public static void main(String arts[]) {
		ExDpProto edp=new ExDpProto();
	}
	ExDpProto(){
		du=new EDPdirectoryUtil();
		String path=du.create_directories();
		reader=new EDPInputXmlReader(xmlfile);
		du.copy_file(xmlfile);
		
		nx=reader.getNx();
		ny=reader.getNy();
		nz=1;										//	2次元の計算。nzの設定値は無視
		thickness=ny;
		exp_fraction=reader.getExp_fraction();
		numlines=reader.getNumlines();
		ratio=reader.getRatio();
				
		random=new Random(reader.getRandom_seed());
		
		cells=new Resist[nx][ny][nz];
		setCells(cells);
		stat=new EDTStatus();
		frame=new EDTFrame(nx,ny,cells,du.getDp(),du.getId());
		frame.setStat(stat);

		profout=new EDPOutputProfile(du,cells);
		
		calculation();
		
		profout.finalize_output();
	}
	void calculation() {
		
		coating();													//	レジスト膜のコーティング（初期化）
		define_grassed_area();										//	ガラス化して不溶化した領域の定義
		
		System.out.print("exposure...");							//	感光過程
		Exposure exposure=new Exposure(cells);						//	従来のシミュレーションとの比較のためのデータを取得する
		exposure.define_exposed_area(numlines, ratio);				//	露光領域の定義			
		exposure.calculation(thickness, exp_fraction, random);		//	このクラスのcellsの状態変わる。共有されている
		System.out.println("done");
		
		Development development=new Development(this);
		development.calculation(profout);
		
		frame.repaint();
		output_summary(exposure,development);	
	}
	void coating() {				//	クラスタの構造を作る必要があるので、セルを1つずつ加えていく
		int num=nx*ny;
		System.out.print("coating...");
		while(num>0) {
			int ix=random.nextInt(nx);
			int iy=random.nextInt(thickness);
			int iz=0;
			if (cells[ix][iy][iz]==null) {
				Resist cell=new Resist(ix,iy,iz);
				cells[ix][iy][iz]=cell;
				new_cell(cell);
				num--;
			}
		}
		System.out.println("done");
	}
	void define_grassed_area() {
		double grassed=reader.getGrassed();
		if (grassed==0.0) return;
		int num=(int)((double)(nx*ny)*grassed);
		while(num>0) {
			int ix=random.nextInt(nx);
			int iy=random.nextInt(thickness);
			int iz=0;
			if (!cells[ix][iy][iz].isGrassed()) {
				cells[ix][iy][iz].setGrassed(true);
				num--;
			}
		}
	}
	void output_summary(Exposure exposure, Development development) {
		String pstr=du.get_pstr();
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(pstr+"summary.txt")));
			out.println("data id              = "+du.getId());
			out.println("packing_ratio        = "+new DecimalFormat("0.00000").format(exp_fraction));
			out.println("height  (percolation)= "+exposure.get_height());
			out.println("height  (dissolution)= "+development.get_height());
			out.println("fraction(percolation)= "+new DecimalFormat("0.00000").format(exposure.get_fraction(nx,thickness,nz)));
			out.println("fraction(dissolution)= "+new DecimalFormat("0.00000").format(development.get_fraction(nx,thickness,nz)));
			out.println();
			
			LinkedList<Integer> exposed_area=exposure.getExposed_area();
			PatternParms pp=development.calc_pattern_height(exposed_area);
			out.println("average height       = "+new DecimalFormat("0.00000").format(pp.getAverage()));
			out.println("sigma                = "+new DecimalFormat("0.00000").format(pp.getSigma()));
			out.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	public EDPdirectoryUtil getDu() { return du; }
	public Resist[][][] getCells() { return cells; }
	public EDTFrame getFrame() { return frame; }
	public Random getRandom() { return random; }
	public EDPInputXmlReader getReader() { return reader; }
	public EDTStatus getStat() { return stat; }
}