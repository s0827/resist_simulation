package exdpproto;

import java.io.*;

import exdptest.*;

public class EDPOutputProfile {			//	セルの分布を出力する
	PrintWriter out;
	Resist[][][] cells;
	EDPOutputProfile(EDPdirectoryUtil du, Resist[][][] cells){
		this.cells=cells;
		String pstr=du.get_pstr();
		try {
			out=new PrintWriter(new BufferedWriter(new FileWriter(pstr+"profile.txt")));
			out.println(du.getId()+"\tdata id");
			out.println(cells.length+"\tnx");
			out.println(cells[0].length+"\tny");
			out.println(cells[0][0].length+"\tnz");
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	public void output(int steps, int total_steps) {
		out.println("steps=\t"+steps+"\ttotal steps=\t"+total_steps);
		for (int iz=0; iz<cells[0][0].length; iz++) {
			for (int iy=0; iy<cells[0].length; iy++) {
				for (int ix=0; ix<cells.length; ix++) {
					String ch=" ";
					Resist cell=cells[ix][iy][iz];
					if (cell!=null) {
						if (cell.isGrassed()) {
							ch="G";						//	Grassed
						}
						else if (cell.isExposed()) {
							if (cell.isSimple_percolation()) {
								ch="A";					// 	Attached
							}
							else {
								ch="E";					//	Exposed
							}
						}
						else {
							ch="U";						//	Unexposed
						}						
					}
					out.print(ch);
				}
				out.println();
			}
			out.println();
		}
	}
	void finalize_output() {
		out.close();
	}
}
