package exdpproto;

import java.awt.*;
import java.util.*;

public class EDPPanel extends Panel {
	EDPRenderer rend;							//	レンダリングはEDPRendereで行う
	int panelwidth,panelheight;	
	LinkedHashMap<String, String[][][]> Data;
	int nx,ny,dp;
	
	String key;			//	選択された項目
	
	EDPPanel(EDPRenderer rend, int panelwidth, int panelheight){
		this.rend=rend;
		rend.set_size(panelwidth, panelheight);
		this.panelwidth=panelwidth;
		this.panelheight=panelheight;
		
		setSize(panelwidth,panelheight);
		setVisible(true);
	}
	public void paint(Graphics gc) {		//	クラスタの情報はない
		rend.draw(gc);
	}
}
