package exdpproto;

import java.io.*;						//	入力データの読み込み
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

public class EDPInputXmlReader {
	double cx,cy,cz;					//	計算領域の大きさ、メッシュ数
	int nx,ny,nz;						//	セルはレジスト膜の中のみに置かれる。または、DLAの計算領域はより広い
	double abs_length,exp_fraction;		//	レジストの物理パラメータ
	double idisprob,xdisprob;			//	未露光／露光領域の溶解確率
	double grassed;						//	ガラス化した領域の割合
	int numlines;						//	パターンの本数
	double ratio;						//	溝の割合
	int startpoint,limitheight;			//	DLAの制御パラメータ
	int random_seed;					//	乱数のシード
	int max_steps,profout_steps;		//	最大ステップ数、プロファイル出力刻み
	
	public EDPInputXmlReader() {}
	public EDPInputXmlReader(String xmlfile) {
		try {
			InputStream is=new FileInputStream(xmlfile);
			DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
			DocumentBuilder builder=factory.newDocumentBuilder();
			Document doc=builder.parse(is);
			Element rootElement=doc.getDocumentElement();

			NodeList geometry_list=rootElement.getElementsByTagName("geometry");
			Node geometry_node=geometry_list.item(0);
			NodeList calc_region_list=((Element)geometry_node).getElementsByTagName("calc_region");
			Node calc_region_node=calc_region_list.item(0);
			NamedNodeMap calc_region_attr=calc_region_node.getAttributes();
			cx=get_double_attr(calc_region_attr,"cx");
			cy=get_double_attr(calc_region_attr,"cy");
			cz=get_double_attr(calc_region_attr,"cz");

			NodeList calc_mesh_list=((Element)geometry_node).getElementsByTagName("calc_mesh");
			Node calc_mesh_node=calc_mesh_list.item(0);
			NamedNodeMap calc_mesh_attr=calc_mesh_node.getAttributes();
			nx=get_int_attr(calc_mesh_attr,"nx");
			ny=get_int_attr(calc_mesh_attr,"ny");
			nz=get_int_attr(calc_mesh_attr,"nz");

			NodeList condition_list=rootElement.getElementsByTagName("condition");
			Node condition_node=condition_list.item(0);
			NodeList resist_list=((Element)condition_node).getElementsByTagName("resist");
			Node resist_node=resist_list.item(0);
			NamedNodeMap resist_attr=resist_node.getAttributes();
			idisprob=get_double_attr(resist_attr,"idisprob");
			xdisprob=get_double_attr(resist_attr,"xdisprob");
			NodeList pattern_list=((Element)condition_node).getElementsByTagName("pattern");
			Node pattern_node=pattern_list.item(0);
			NamedNodeMap pattern_attr=pattern_node.getAttributes();
			numlines=get_int_attr(pattern_attr,"numlines");
			ratio=get_double_attr(pattern_attr,"ratio");
			NodeList grassed_list=((Element)condition_node).getElementsByTagName("grassed");
			Node grassed_node=grassed_list.item(0);
			NamedNodeMap grassed_attr=grassed_node.getAttributes();
			grassed=get_double_attr(grassed_attr,"fraction");
			
			NodeList exposure_list=((Element)condition_node).getElementsByTagName("exposure");
			Node exposure_node=exposure_list.item(0);
			NamedNodeMap exposure_attr=exposure_node.getAttributes();
			abs_length=get_double_attr(exposure_attr,"abs_length");
			exp_fraction=get_double_attr(exposure_attr,"exp_fraction");
			
			NodeList dla_list=((Element)condition_node).getElementsByTagName("dla");
			Node dla_node=dla_list.item(0);
			NamedNodeMap dla_attr=dla_node.getAttributes();
			startpoint=get_int_attr(dla_attr,"startpoint");
			limitheight=get_int_attr(dla_attr,"limitheight");
			
			NodeList control_list=rootElement.getElementsByTagName("calc_control");
			Node control_node=control_list.item(0);
			NodeList calc_list=((Element)control_node).getElementsByTagName("calc");
			Node calc_node=calc_list.item(0);
			NamedNodeMap calc_attr=calc_node.getAttributes();
			random_seed=get_int_attr(calc_attr,"random_seed");
			max_steps=get_int_attr(calc_attr,"max_steps");
			profout_steps=get_int_attr(calc_attr,"profout_steps");
			
			is.close();			
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	double get_double_attr(NamedNodeMap map, String key) {		//	色々なプログラムを作ったら共通化する
		double d=0.0;
		String s=get_attr(map,key);
		if (s!=null) d=Double.parseDouble(s);
		return d;
	}
	int get_int_attr(NamedNodeMap map, String key) {
		int i=0;
		String s=get_attr(map,key);
		if (s!=null) i=Integer.parseInt(s);
		return i;
	}
	String get_attr(NamedNodeMap map, String key) {
		String s=null;
		Node node=map.getNamedItem(key);
		if (node!=null) s=node.getNodeValue();
		return s;
	}
	public double get_dx() { return cx/(double)nx; }
	public double get_dy() { return cy/(double)ny; }
	public double get_dz() { return cz/(double)nz; }
	
	public double getCx() { return cx; }
	public double getCy() { return cy; }
	public double getCz() { return cz; }
	public int getNx() { return nx; }
	public int getNy() { return ny; }
	public int getNz() { return nz; }
	public double getIdisprob() { return idisprob; }
	public double getXdisprob() { return xdisprob; }
	public double getGrassed() { return grassed; }
	public int getNumlines() { return numlines; }
	public double getRatio() { return ratio; }
	public double getAbs_length() { return abs_length; }
	public double getExp_fraction() { return exp_fraction; }
	public int getStartpoint() { return startpoint; }
	public int getLimitheight() { return limitheight; }
	public int getRandom_seed() { return random_seed; }
	public int getMax_steps() { return max_steps; }
	public int getProfout_steps() { return profout_steps; }
}
