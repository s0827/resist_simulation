package exdpproto;

import java.awt.*;
import java.util.*;

public class EDPRenderer {
	int panelwidth, panelheight;
	int nx,ny,dp;
	LinkedHashMap<String, String[][][]> Data;
	String id,key;
	
	EDPRenderer(int nx, int ny, int dp, LinkedHashMap<String, String[][][]> Data){		
		this.nx=nx;
		this.ny=ny;
		this.dp=dp;
		this.Data=Data;
	}
	void set_size(int panelwidth, int panelheight) {
		this.panelwidth=panelwidth;
		this.panelheight=panelheight;
	}
	void setId(String id) { this.id=id; }
	void setKey(String key) { this.key=key; }
	
	void draw(Graphics gc) {
		gc.setColor(Color.WHITE);
		gc.fillRect(0, 0, panelwidth, panelheight);

		String[][][] S=Data.get(key);
		
		for (int ix=0; ix<nx; ix++) {				//	状態に応じた色で塗りつぶす
			for (int iy=0; iy<ny; iy++) {
				int x=ix*dp;
				int y=panelheight-(iy+1)*dp;
				if (S[ix][iy][0].equals("A")) {
					gc.setColor(Color.WHITE);
				}
				else if (S[ix][iy][0].equals("E")) {
					gc.setColor(Color.LIGHT_GRAY);
				}
				else if (S[ix][iy][0].equals("G")) {
					gc.setColor(Color.DARK_GRAY);
				}
				else if (S[ix][iy][0].equals("U")) {
					gc.setColor(Color.GRAY);
				}
				else {
					gc.setColor(Color.WHITE);
				}
				gc.fillRect(x, y, dp, dp);
			}
		}
		gc.setColor(Color.BLACK);
		for (int ix=0; ix<nx-1; ix++) {
			for (int iy=0; iy<ny; iy++) {
				int x=(ix+1)*dp;
				int y0=panelheight-(iy+1)*dp;
				int y1=panelheight-iy*dp;
				if (!S[ix][iy][0].equals(S[ix+1][iy][0])) {
					gc.drawLine(x, y0, x, y1);
				}					
			}
		}
		for (int iy=0; iy<ny-1; iy++) {
			for (int ix=0; ix<nx; ix++) {
				int x0=ix*dp;
				int x1=(ix+1)*dp;
				int y=panelheight-(iy+1)*dp;
				if (!S[ix][iy][0].equals(S[ix][iy+1][0])) {
					gc.drawLine(x0, y, x1, y);
				}					
			}
		}
		for (int ix=0; ix<nx; ix++) {
			int x0=ix*dp;
			int x1=(ix+1)*dp;
			int y=0;
			if (!S[ix][ny-1][0].equals(" ")) {
				gc.drawLine(x0, y, x1, y);				
			}
		}
		gc.drawLine(0,panelheight-1,panelwidth,panelheight-1);
		gc.setColor(Color.BLACK);
		String str="";
		if (id.length()>0) str="["+id+"] ";
		gc.drawString(str+"step="+key, 5, 20);
	}
	int getPanelwidth() { return panelwidth; }
	int getPanelheight() { return panelheight; }
}
