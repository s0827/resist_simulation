package exdpproto;

import java.io.*;
import java.util.*;

import percolation.*;

public class EDPdirectoryUtil extends DirectoryUtil {		//	マシン固有のパラメータを得るためにDirectoryUtilを拡張
//	String code="XD";										//	設定内容が異なるが、定義は主に親クラスによる
//	String sys_id="0";										//	これまでのDirectroyUtilと違うところだけ実装
	String m_dir="/Users/a_sasaki/";
//	String master_dir=m_dir+"data/";
//	String org_dir="originals/";
//	String d_dir;
//	String original,id;
	int dp;													//	グラフィクスの制御用。ホストの解像度の指定
	EDPdirectoryUtil(){
		code="XD";
		try {
			BufferedReader in=new BufferedReader(new FileReader("host.txt"));
			sys_id=get_first_word(in.readLine());			//	最初の語を取得
			m_dir=get_first_word(in.readLine());
			dp=Integer.parseInt(get_first_word(in.readLine()));
			in.close();
			master_dir=m_dir+"data/";						//	ディレクトリ名を決定
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	String get_first_word(String line) {
		String str="";
		StringTokenizer tokens=new StringTokenizer(line,"\t ");
		if (tokens.hasMoreTokens()) str=tokens.nextToken();
		return str;
	}
	int getDp() { return dp; }
}
