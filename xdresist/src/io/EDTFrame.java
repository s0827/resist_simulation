package io;

import java.awt.*;

import exdptest.Resist;
import percolation.TWPanel;

public class EDTFrame extends Frame {
	int dp=24, offset=20, legend=25;
	int framewidth,frameheight;
	EDTPanel panel;
	EDTStatus stat;
	String id="";
	
	public EDTFrame(int nx, int ny, Resist[][][] cells, int dp, String id) {
		this.dp=dp;
		this.id=id;
		initialize(nx,ny,cells);
	}
	public EDTFrame(int nx, int ny, Resist[][][] cells) {
		initialize(nx,ny,cells);
	}
	void initialize(int nx, int ny, Resist[][][] cells) {
		int panelwidth=nx*dp;
		int panelheight=ny*dp;
		framewidth=panelwidth;
		frameheight=panelheight+offset+legend;
		setSize(framewidth,frameheight);
		setVisible(true);
		setLayout(null);
		panel=new EDTPanel(panelwidth,panelheight);
		panel.setLocation(0,offset);
		add(panel);

		panel.put_parms(nx,ny,dp);				//	パネルにデータを渡す
		panel.setCells(cells);		
	}
	public void setStat(EDTStatus stat) { this.stat=stat; } 
	public void paint(Graphics gc) {
		gc.setColor(Color.WHITE);
		gc.fillRect(0, 0, framewidth, frameheight);
		
		if (panel!=null) panel.repaint();
		
		if (stat!=null) {
			gc.setColor(Color.BLACK);
			String str="";
			if (id.length()>0) str="["+id+"] ";
			gc.drawString(str+"step="+stat.getStep(), 5, frameheight-7);			
		}
	}
}
